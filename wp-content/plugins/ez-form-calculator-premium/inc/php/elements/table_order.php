<?php

class Ezfc_Element_Table_order extends Ezfc_Element {
	public function get_output() {
		global $element_data;
		global $element_vars;
		global $output;

		$el_text  = "";
		$add_attr = "";

		$this->element_js_vars["table_order"] = array();
		$element_data = $this->data;

		$this->element_js_vars["table_order_loop_function"] = Ezfc_Functions::get_object_value($element_data, "table_order_loop_function", "");

		$element_options = $this->frontend->get_options_source($this->data, $this->id, $this->options);
		$output = $this->output;

		$el_text .= "<div class='{$this->data->class} ezfc-element ezfc-element-table_order' id='{$this->output["element_child_id"]}' {$this->output["style"]} {$this->output["required"]} {$add_attr}>";

		$el_text .= "<table class='ezfc-element-table_order-table'>";

		// check for images first
		$has_image = false;
		foreach ($element_options as $n => $option) {
			if (!empty($option->image)) {
				$has_image = true;
				break;
			}
		}

		foreach ($element_options as $n => $option) {
			$input_id = "{$this->output["element_child_id"]}-{$n}";
			$price_column_id = $input_id . "-price";
			$row_subtotal_id = $input_id . "-subtotal";

			$min = property_exists($option, "min") ? $option->min : 0;
			$max = property_exists($option, "max") ? $option->max : 0;
			$input_value = max(0, $min);

			$add_data = "";

			// option ID
			if (!empty($option->id)) {
				$add_data .= " data-optionid='" . esc_attr($option->id) . "'";
			}
			// disabled
			if (!empty($option->disabled)) {
				$add_data .= " disabled='disabled'";
			}

			$item_price = $this->frontend->number_format($option->value, $this->data);

			$element_vars = array(
				"add_data"        => $add_data,
				"has_image"       => $has_image,
				"id"              => $option->id,
				"index"           => $n,
				"input_id"        => $input_id,
				"input_value"     => $input_value,
				"item_price"      => $item_price,
				"min"             => $min,
				"max"             => $max,
				"option"          => $option,
				"price_column_id" => $price_column_id,
				"subtotal_id"     => $row_subtotal_id,
				"value"           => $option->value
			);
			// js
			$this->element_js_vars["table_order"][$n] = $element_vars;

			$el_text .= $this->frontend->get_template("elements/table_order-loop");
		}

		$el_text .= "</table>";
		$el_text .= "</div>";

		return $el_text;
	}

	/**
		returns the formatted selected option values
	**/
	public function get_summary_value_options($return_type = "text") {
		// invalid data
		if (!is_array($this->submission_value)) return "";

		$return_value = array();

		// check for options source
		$element_values = (array) $this->frontend->get_options_source($this->data, $this->id, $this->options);

		foreach ($element_values as $i => $option) {
			// invalid data or conditionally hidden
			if (!isset($this->submission_value[$i]) || strpos($this->submission_value[$i], "__HIDDEN__") !== false) continue;

			if (($this->submission_value[$i] == 0 && $this->data->show_empty_values_in_email == 1) || ($this->submission_value[$i] != 0 && $this->data->show_empty_values_in_email == 0) || $this->data->show_empty_values_in_email == 1) {
				$loop_item_val = (int) $this->submission_value[$i];
				$loop_item_val_calculated = $loop_item_val * $option->value;
				$loop_item_val_formatted = $this->format_number($loop_item_val_calculated, true);

				$return_value[] = "{$loop_item_val}x " . esc_html($option->text) . " = {$loop_item_val_formatted}";
			}
		}

		return implode("<br>", $return_value);
	}
}