<?php

class Ezfc_Element_Hr extends Ezfc_Element {
	public function get_output() {
		$el_text = "<hr class='{$this->data->class}' {$this->output["style"]} />";

		return $el_text;
	}
}