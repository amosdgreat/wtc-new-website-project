<?php

class Ezfc_Element_Stepstart extends Ezfc_Element {
	public function get_output() {
		$this->step = true;
		$el_text    = "";
					
		$step_class = "ezfc-step ezfc-step-start {$this->data->wrapper_class}";
		if ($this->add_vars["current_step"] == 0) $step_class .= " ezfc-step-active";

		$el_text = "<div class='{$step_class}' data-step='{$this->add_vars["current_step"]}' data-id='{$this->element->id}' id='ezfc-element_{$this->element->id}'>";

		if (!empty($this->data->title) && $this->options["step_use_titles"] == 0) {
			$el_text .= "<div class='ezfc-step-title {$this->data->class}'>{$this->data->title}</div>";
		}

		return $el_text;
	}

	public function prepare_label() {
		$this->default_label = "";
	}
}