<?php

class Ezfc_Element_Repeatable_form extends Ezfc_Element {
	public $elements = array();

	public function init() {
		if ($this->data->form_id == 0) return;

		$this->elements = Ezfc_Functions::array_index_key($this->frontend->form_elements_get($this->data->form_id), "id");
		//$this->frontend->form_elements = array_merge($this->frontend->form_elements, $this->elements);
	}

	public function get_output() {
		if ($this->data->form_id == 0) return;

		$form_output = $this->frontend->get_output($this->data->form_id);

		return $form_output;
	}
}