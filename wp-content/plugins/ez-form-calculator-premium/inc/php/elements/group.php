<?php

class Ezfc_Element_Group extends Ezfc_Element {
	public function get_output() {
		$el_text  = "";
					
		$collapse_group_elements_style = "";

		//$element_js_vars["repeatable"] = $this->data->repeatable;

		if (!empty($this->data->collapsible)) {
			$collapse_group_elements_style = empty($this->data->expanded) ? "display: none;" : "";
			$collapse_icon                 = empty($this->data->expanded) ? "fa-chevron-circle-right" : "fa-chevron-circle-down";
			$this->element_css_classes    .= " ezfc-group-collapsible";
			$this->element_css_classes    .= !empty($this->data->expanded) ? " ezfc-group-active" : "";

			// collapse title + toggle handler
			$el_text .= "<div class='ezfc-collapse-title-wrapper'>";
			$el_text .= "	<span class='ezfc-collapse-icon'><i class='fa {$collapse_icon}'></i></span>";
			$el_text .= "	<span class='ezfc-collapse-title'>{$this->data->title}</span>";
			$el_text .= "</div>";
		}
			
		// group elements wrapper
		//$el_text .= "<div class='ezfc-group-elements' style='{$collapse_group_elements_style}'></div>";
		$el_text .= "<div class='ezfc-group-elements' style='{$collapse_group_elements_style}'>"; // closing div will be added in build function

		/*if ($this->data->repeatable == 1) {
			$el_text .= "</div>";
			$el_text .= "<div class='ezfc-group-repeatable-wrapper'><button class='ezfc-group-repeat' data-group_repeat_id='{$element->id}'>Repeat</button></div>";
			$el_text .= "<div>";
		}*/

		return $el_text;
	}

	public function get_label() {
		return "";
	}
}