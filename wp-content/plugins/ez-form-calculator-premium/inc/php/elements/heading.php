<?php

class Ezfc_Element_Heading extends Ezfc_Element {
	public function get_output() {
		$el_text  = "";
		$el_text .= "<{$this->data->tag} class='{$this->data->class}' {$this->output["style"]}>{$this->data->title}</{$this->data->tag}>";

		return $el_text;
	}

	public function get_label() {
		return "";
	}
}