<?php

class Ezfc_Element_Html extends Ezfc_Element {
	public function get_output() {
		$el_text = "";
		$el_text .= "<div>";

		$tmp_text = wp_specialchars_decode(stripslashes($this->data->html));

		// replace magic values
		$tmp_text = str_replace("__form_id__", $this->form->id, $tmp_text);
		$tmp_text = str_replace(array("&quot;", "&#39;"), "'", $tmp_text);

		$this->element_js_vars["replace_placeholders_show_zero"] = Ezfc_Functions::get_object_value($this->data, "replace_placeholders_show_zero", 1);

		// shortcode
		if (!empty($this->data->do_shortcode)) {
			$tmp_text = do_shortcode($tmp_text);
		}
		// placeholders
		if (!empty($this->data->replace_placeholders)) {
			$tmp_text = $this->frontend->get_listen_placeholders($this->data, $tmp_text);
		}

		// linebreaks / content filter
		if (!empty($this->data->add_linebreaks)) {
			$tmp_text = wpautop($tmp_text);
			$tmp_text = $this->frontend->apply_content_filter($tmp_text);
		}

		// convert encoding
		if (isset($this->frontend->global_options["html_convert_encoding"]["from"]) && isset($this->frontend->global_options["html_convert_encoding"]["to"])) {
			$encoding_from = trim($this->frontend->global_options["html_convert_encoding"]["from"]);
			$encoding_to   = trim($this->frontend->global_options["html_convert_encoding"]["to"]);

			if (!empty($encoding_from) && !empty($encoding_to)) {
				$tmp_text = mb_convert_encoding($tmp_text, $encoding_to, $encoding_from);
			}
		}

		if ($this->frontend->global_options["html_force_convert_utf8"] == 1) {
			$tmp_text = utf8_encode($tmp_text);
		}

		$el_text .= $tmp_text;

		$el_text .= "<input class='ezfc-element-html' name='{$this->output["element_name"]}' type='hidden' value='1' />";
		$el_text .= "</div>";

		return $el_text;
	}

	public function set_submission_value($value) {
		parent::set_submission_value($value);

		$value = $this->data->html;

		return $value;
	}

	public function submission_show_in_email() {
		// skip html elements since it is disabled in the form options
		if ($this->options["email_show_html_elements"] == 0) return false;

		// todo: check
		return true;
	}
}