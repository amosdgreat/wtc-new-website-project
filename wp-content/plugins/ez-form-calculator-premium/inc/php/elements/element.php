<?php

class Ezfc_Element {
	// frontend class
	public $frontend;
	// form options
	public $options;
	// element object
	public $element;
	// element id
	public $e_id;
	// form object
	public $form;
	// form element id
	public $id;
	// form element data
	public $data;
	// output
	public $output = array();
	// submission value for summary/emails
	public $submission_value = "";
	public $calculated_submission_value = 0;

	public function __construct($form, $element, $id = null, $type = "input") {
		$this->frontend = Ezfc_frontend::instance();

		// form
		$this->form = $form;
		// element
		$this->element = $element;
		// element id
		$this->e_id = $element->e_id;
		// form element id
		$this->id = $id;
		// element type
		$this->type = $type;

		// default vars
		$this->add_vars            = array();
		$this->element_css_classes = "";
		$this->element_js_vars     = array();
		$this->step                = false;

		if (property_exists($element, "data")) {
			$element_data = json_decode($element->data);
			$this->set_element_data($element_data);
		}
	}

	// called after data initialization
	public function init() {
	}

	public function get_element_data() {
		return $this->data;
	}

	public function set_element_data($data) {
		$this->data = $data;
	}

	public function set_form($form) {
		$this->form = $form;
	}

	public function set_form_options($options) {
		$this->options = $options;
	}

	// frontend output
	public function prepare_output($options, $add_vars = array()) {
		$this->options = $options;

		// wrapper id
		$this->output["element_id"] = "ezfc_element-{$this->id}";
		// input id
		$this->output["element_child_id"] = $this->output["element_id"] . "-child";
		// input name
		$this->output["element_name"] = $options["hard_submit"] == 1 ? "ezfc_element[{$this->data->name}]" : "ezfc_element[{$this->id}]";

		// additional vars
		$this->add_vars = $add_vars;

		// check if required
		$this->prepare_required();
		// prepare label
		$this->prepare_label();
		// prepare factor value
		$this->prepare_factor();
		// prepare styles
		$this->prepare_styles();
		// prepare value
		$this->prepare_value();
	}

	public function prepare_factor() {
		$factor = Ezfc_Functions::get_object_value($this->data, "factor", 1);

		if ($factor == "") {
			$factor = 1;
		}

		$factor = do_shortcode($factor);

		// override factor
		if (property_exists($this->data, "factor")) {
			$this->element_js_vars["factor"] = $factor;
		}

		$this->factor           = $factor;
		$this->output["factor"] = "data-factor='" . esc_attr($factor) . "'";
	}

	public function prepare_label() {
		// data label
		$el_data_label = "";

		// trim labels
		if (property_exists($this->data, "label")) {
			$tmp_label = trim(htmlspecialchars_decode($this->data->label));

			// todo: cache option
			if (get_option("ezfc_allow_label_shortcodes", 0)) {
				$tmp_label = do_shortcode($tmp_label);
			}

			// placeholders
			$tmp_label = $this->frontend->get_listen_placeholders($this->data, $tmp_label);

			$el_data_label .= $tmp_label;
		}

		// element description
		if (!empty($this->data->description)) {
			$element_description = "<span class='ezfc-element-description ezfc-element-description-{$this->options["description_label_position"]}' data-ezfctip='" . esc_attr($this->data->description) . "'></span>";

			$element_description = apply_filters("ezfc_element_description", $element_description, $this->data->description);

			if ($this->options["description_label_position"] == "before") {
				$el_data_label = $element_description . $el_data_label;
			}
			else {
				$el_data_label = $el_data_label . $element_description;
			}
		}

		// add whitespace for empty labels
		if ($el_data_label == "" && $this->options["add_space_to_empty_label"] == 1) {
			$el_data_label .= " &nbsp;";
		}

		// additional styles
		// todo: globalize / cache option
		$css_label_width = get_option("ezfc_css_form_label_width");
		$css_label_width = empty($css_label_width) ? "" : "style='width: {$css_label_width}'";

		// default label
		$this->default_label = "<label class='ezfc-label' for='{$this->output["element_child_id"]}' {$css_label_width}>" . $el_data_label . "{$this->output["required_char"]}</label>";
	}

	public function prepare_required() {
		$required_check = Ezfc_Functions::get_object_value($this->data, "required", 0);

		$required      = "";
		$required_char = "";

		if ($required_check) {
			$required = "required";

			if ($this->options["show_required_char"] != 0) {
				$required_char = " <span class='ezfc-required-char'>*</span>";
			}
		}

		// is this element required?
		$this->required = $required_check;
		// text to be added in the input element
		$this->output["required"] = $required;
		// required char
		$this->output["required_char"]  = $required_char;
	}

	public function prepare_styles() {
		// inline style
		$this->output["style"] = "";
		if (!empty($this->data->style)) {
			$this->output["style"] = "style='{$this->data->style}'";
		}

		// options container class
		$this->output["options_container_class"] = "";
		// flex
		if (Ezfc_Functions::get_object_value($this->data, "flexbox", 0) == 1) {
			$this->output["options_container_class"] .= " ezfc-flexbox";
		}
	}

	public function prepare_value() {
		global $post;
		global $product; // woocommerce product (perhaps empty)

		// modify value
		if (!property_exists($this->data, "value")) return;

		// WC attribute
		if (!empty($this->data->value_attribute) && !empty($product) && method_exists($product, "get_attribute")) {
			$this->data->value = $product->get_attribute($this->data->value_attribute);
		}

		// acf
		if (strpos($this->data->value, "acf:") !== false && function_exists("get_field")) {
			$tmp_array = explode(":", $this->data->value);
			$this->data->value = get_field($tmp_array[1]);
		}

		// postmeta
		else if (strpos($this->data->value, "postmeta:") !== false) {
			$tmp_array = explode(":", $this->data->value);
			$this->data->value = get_post_meta(get_the_ID(), $tmp_array[1], true);
		}

		// woocommerce product attribute via this->data->value
		else if (strpos($this->data->value, "wc:") !== false && !empty($product) && method_exists($product, "get_attribute")) {
			$tmp_array = explode(":", $this->data->value);
			$this->data->value = $product->get_attribute($tmp_array[1]);
		}

		// php function
		else if (strpos($this->data->value, "php:") !== false) {
			$tmp_array = explode(":", $this->data->value);
			if (!empty($tmp_array[1]) && function_exists($tmp_array[1])) {
				$this->data->value = htmlspecialchars($tmp_array[1]($element, $this->data, $this->options, $this->form->id), ENT_QUOTES, "UTF-8");
			}
		}

		// replace placeholder values
		$replace_values = $this->frontend->get_frontend_replace_values();
		foreach ($replace_values as $replace => $replace_value) {
			$this->data->value = str_ireplace("{{" . $replace . "}}", $replace_value, $this->data->value);
		}

		// random number
		if ($this->data->value == "__rand__" && property_exists($this->data, "min") && is_numeric($this->data->min) && property_exists($this->data, "max") && is_numeric($this->data->max)) {
			$this->data->value = function_exists("mt_rand") ? mt_rand($this->data->min, $this->data->max) : rand($this->data->min, $this->data->max);
		}

		// shortcode value
		if (get_option("ezfc_allow_value_shortcodes", 1)) {
			$this->data->value = do_shortcode($this->data->value);
		}

		// quotes
		$this->data->value = htmlspecialchars_decode($this->data->value, ENT_QUOTES);
	}

	/**
		get css classes for element wrapper
	**/
	public function get_element_css($css_classes) {
		return $css_classes . " " . $this->element_css_classes;
	}

	/**
		get element js vars
	**/
	public function get_element_js_vars($element_js_vars) {
		$element_js_vars = array_merge($element_js_vars, $this->element_js_vars);

		return $element_js_vars;
	}

	/**
		get external scripts
	**/
	public function enqueue_scripts() {}

	/**
		get icon
	**/
	public function get_icon() {
		$icon = "";

		if (!empty($this->data->icon)) {
			$icon = "<i class='ezfc-element-icon fa {$this->data->icon}'></i>";
			// add icon class
			$this->data->class .= " ezfc-has-icon";
		}

		return $icon;
	}

	/**
		get label
	**/
	public function get_label() {
		return $this->default_label;
	}

	/**
		get default output (input field)
	**/
	public function get_output() {
		$el_text  = "";
		$add_attr = "";

		// readonly
		if (!empty($this->data->read_only)) $add_attr .= " readonly";
		// max length
		if (property_exists($this->data, "max_length") && $this->data->max_length != "") $add_attr .= " maxlength='{$this->data->max_length}'";
		// autocomplete
		if (property_exists($this->data, "autocomplete") && $this->data->autocomplete == 0) $add_attr .= " autocomplete='false'";

		// value
		$value = "";
		if (property_exists($this->data, "value")) {
			$add_attr .= " data-initvalue='" . esc_attr($this->data->value) . "'";
			$value     = $this->data->value;
		}
		// readonly
		if (property_exists($this->data, "read_only") && $this->data->read_only == 1) {
			$add_attr .= " readonly";
		}

		// placeholder
		$placeholder = Ezfc_Functions::get_object_value($this->data, "placeholder", "");

		// tel
		$input_type = empty($this->data->is_telephone_nr) ? "text" : "tel";

		$css_classes = property_exists($this->data, "class") ? $this->data->class : "";

		$el_text .= "<input	class='{$css_classes} ezfc-element ezfc-element-input' {$this->output["factor"]} id='{$this->output["element_child_id"]}' name='{$this->output["element_name"]}' placeholder='{$placeholder}' type='{$input_type}' value='{$value}' {$add_attr} {$this->output["style"]} {$this->output["required"]} />";

		return $el_text;
	}

	/**
		set submission value
	**/
	public function set_submission_value($value) {
		if (is_array($value)) {
			$value = array_map("stripslashes", $value);
		}
		else {
			$value = stripslashes($value);
		}

		$this->submission_value = $value;

		return $value;
	}

	/**
		set calculated submission value
	**/
	public function set_calculated_submission_value($value) {
		$this->calculated_submission_value = $value;
	}

	/**
		check if this element should be included in submission table
	**/
	public function submission_show_in_email() {
		// conditionally hidden
		if ($this->submission_value == "__HIDDEN__") return false;
		if (is_array($this->submission_value) && isset($this->submission_value[0]) && strpos($this->submission_value[0], "__HIDDEN__") !== false) return false;

		// check if element will be shown in email
		$show_in_email = false;
		if (property_exists($this->data, "show_in_email")) {
			// always show
			if ($this->data->show_in_email == 1) {
				$show_in_email = true;
			}
			// show if not empty
			else if ($this->data->show_in_email == 2) {
				$show_in_email = Ezfc_conditional::value_not_empty($this->submission_value);
			}
			// show if not empty and not 0
			else if ($this->data->show_in_email == 3) {
				$show_in_email = Ezfc_conditional::value_not_empty_not_zero($this->submission_value);
			}
			// conditional show
			else if ($this->data->show_in_email == 4) {
				// invalid -> show nevertheless
				if (empty($this->data->show_in_email_cond)) {
					$show_in_email = true;
				}
				// loop through conditions
				else if (!empty($this->data->show_in_email_cond) && is_array($this->data->show_in_email_cond)) {
					$show_in_email_index = true;
					foreach ($this->data->show_in_email_cond as $i => $cond_element_id) {
						$selected_id = in_array($this->data->show_in_email_operator[$i], array("selected_id", "not_selected_id", "selected_count", "not_selected_count", "selected_count_gt", "selected_count_lt", "in", "not_in"));

						// check if data was submitted
						if (isset($this->frontend->submission_data["raw_values"][$cond_element_id])) {
							// check for selected id
							if ($selected_id) {
								$compare_value = $this->get_selected_option_id($cond_element_id, $frontend->submission_data["raw_values"][$cond_element_id]);
							}
							else {
								$compare_value = $this->frontend->submission_data["raw_values"][$cond_element_id];
							}
						}
						// data wasn't submited -> checkbox or radio options
						else {
							if ($selected_id) $compare_value = array();
							else $compare_value = 0;
						}

						// visibility check
						if (in_array($this->data->show_in_email_operator[$i], array("hidden", "visible"))) {
							$compare_value = !empty($this->frontend->submission_data["elements_visible"][$this->id]);
						}

						$show_in_email_index_compare = Ezfc_conditional::check_operator($compare_value, $this->data->show_in_email_value[$i], $this->data->show_in_email_operator[$i], $compare_value);

						// set flag to false
						if (!$show_in_email_index_compare) $show_in_email_index = false;
					}

					$show_in_email = $show_in_email_index;
				}
			}
			// show if visible
			else if ($this->data->show_in_email == 5) {
				$show_in_email = !empty($this->frontend->submission_data["elements_visible"][$this->id]);
			}
			// show if visible and not empty
			else if ($this->data->show_in_email == 6) {
				$show_in_email = !empty($this->frontend->submission_data["elements_visible"][$this->id]) && Ezfc_conditional::value_not_empty($this->submission_value);
			}
			// show if visible and not empty and not zero
			else if ($this->data->show_in_email == 7) {
				$show_in_email = !empty($this->frontend->submission_data["elements_visible"][$this->id]) && Ezfc_conditional::value_not_empty_not_zero($this->submission_value);
			}
		}

		return $show_in_email;
	}

	/**
		get summary label
	**/
	public function get_summary_label() {
		// what label to show. default = name
		$label_choice = $this->data->name;

		if (property_exists($this->data, "show_in_email_label")) {
			if ($this->data->show_in_email_label == "label" && property_exists($this->data, "label")) {
				$label_choice = $this->data->label;
			} 
			else if ($this->data->show_in_email_label == "custom" && property_exists($this->data, "show_in_email_label_custom")) {
				$label_choice = $this->data->show_in_email_label_custom;
			}
		}

		return $label_choice;
	}

	/**
		returns the formatted calculated value
	**/
	public function get_summary_value_calculated() {
		// no calculation value
		if (!property_exists($this->data, "is_number") || $this->data->is_number == 0) return "";

		return $this->format_number($this->calculated_submission_value, true);
	}

	/**
		returns the formatted submitted value
	**/
	public function get_summary_value_formatted() {
		if (property_exists($this->data, "options")) return $this->get_summary_value_options();

		// skip email double check
		if (strpos($this->id, "email_check") !== false) return "";

		// skip hidden field by conditional logic
		if ($this->is_conditionally_hidden()) return "";

		$summary_value = $this->submission_value;

		if (Ezfc_Functions::get_object_value($this->data, "is_currency") == 1 && $this->options["format_currency_numbers_elements"] == 1) {
			$summary_value = $this->format_number($this->submission_value);
		}

		if (is_array($summary_value)) {
			$summary_value = implode(",", $summary_value);
		}

		//$summary_value = $this->wrap_text($summary_value);

		return $summary_value;
	}

	public function get_summary_value_image() {
		return $this->get_summary_value_options("image");
	}

	/**
		returns the formatted selected option values
	**/
	public function get_summary_value_options($return_type = "text") {
		$return_value = "";

		// check for options source
		$element_values = (array) $this->frontend->get_options_source($this->data, $this->id, $this->options);

		if (!is_array($this->submission_value) && isset($element_values[$this->submission_value])) {
			// check and return image URL
			if ($return_type == "image" && !empty($element_values[$this->submission_value]->image)) {
				$return_value = "<img src='{$element_values[$this->submission_value]->image}' alt='' />";
			}
			// default text
			else {
				$return_value = esc_html($element_values[$this->submission_value]->text);
			}
		}

		return $return_value;
	}

	/**
		format number
	**/
	public function format_number($value, $is_normalized = false) {
		// not a number
		if (Ezfc_Functions::get_object_value($this->data, "is_number") == 0) return $value;

		$value_old = $value;

		// currency
		if (Ezfc_Functions::get_object_value($this->data, "is_currency") == 1 && $this->options["format_currency_numbers_elements"] == 1 && !$is_normalized) {
			$value = $this->frontend->normalize_value($value);
		}

		return $this->frontend->number_format($value, $this->data);
	}

	// ignore data settings
	public function format_number_force($value) {
		return $this->frontend->number_format($value, $this->data);
	}

	/**
		check if the element was conditionally hidden
	**/
	public function is_conditionally_hidden() {
		$value_check = $this->submission_value;

		if (is_array($value_check) && isset($value_check[0])) {
			$value_check = $value_check[0];
		}

		return strpos($value_check, "__HIDDEN__") !== false && empty($this->data->calculate_when_hidden);
	}

	/**
		wrap text_before/text_after
	**/
	public function wrap_text($value) {
		if (!property_exists($this->data, "text_before")) return $value;


		if (Ezfc_Functions::get_object_value($this->data, "is_number", 0) == 1) return $value;

		return $this->data->text_before . $value . $this->data->text_after;
	}
}