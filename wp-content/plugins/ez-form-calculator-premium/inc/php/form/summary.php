<?php

class Ezfc_summary {
	public $frontend;
	public $form_elements          = array();
	public $form_options           = array();
	public $form_values_calculated = array();
	public $form_values            = array();
	public $form_values_raw        = array();
	public $theme                  = "default";
	public $total                  = 0;

	public function __construct() {
		$this->frontend = Ezfc_frontend::instance();
	}

	public function get_summary() {
		global $summary_data;

		$summary_data = array(
			"columns" => array(
				$this->form_options["summary_column_1"],
				$this->form_options["summary_column_2"],
				$this->form_options["summary_column_3"]
			),
			"form_options" => $this->form_options,
			"total" => $this->total,
			"total_formatted" => $this->frontend->number_format($this->total)
		);

		// result theme
		$theme_output = array(
			"header"  => $this->frontend->get_template("result/{$this->theme}/header"),
			"content" => "",
			"footer"  => $this->frontend->get_template("result/{$this->theme}/footer")
		);

		$content = array();
		$i = 0;
		foreach ($this->form_elements as $fe_id => $element) {
			$value = isset($this->form_values[$fe_id]) ? $this->form_values[$fe_id] : false;
			$calculated_value = isset($this->form_values_calculated[$fe_id]) ? $this->form_values_calculated[$fe_id] : 0;

			$content_loop = $this->get_loop_item($element, $value, $calculated_value, $i);

			// add to table
			if ($content_loop !== false) {
				$content[] = $content_loop;
				$i++;
			}
		}

		$theme_output["content"] = implode("", $content);

		return $theme_output;
	}

	public function get_loop_item($element, $value, $calculated_value, $i) {
		global $loop_data;

		// sanitize
		$calculated_value = Ezfc_Functions::sanitize($calculated_value);
		$value = Ezfc_Functions::sanitize($value);

		// set/retrieve modified submission value
		$calculated_value = $element->set_calculated_submission_value($calculated_value);
		$value = $element->set_submission_value($value);

		// do not show element in email
		if (!$element->submission_show_in_email()) return false;

		$loop_data = array(
			"even"             => $i % 2 == 0,
			"label"            => $element->get_summary_label(),
			"text_column_1"    => $this->get_text_column_value(Ezfc_Functions::get_object_value($element->data, "email_text_column_1", "submission_value"), $element),
			"text_column_2"    => $this->get_text_column_value(Ezfc_Functions::get_object_value($element->data, "email_text_column_2", "calculated_value"), $element),
			"value_calculated" => $element->get_summary_value_calculated(), // old: total_out_string
			"value_formatted"  => $element->get_summary_value_formatted(), // old: $value_out_simple_html
			"value_raw"        => isset($this->form_values[$element->id]) ? $this->form_values[$element->id] : false,
			"values"           => $value
		);

		$loop_content = $this->frontend->get_template("result/{$this->theme}/loop");

		return $loop_content;
	}

	public function get_text_column_value($text_type, $element) {
		$return_value = "";

		switch ($text_type) {
			case "blank": $return_value = "";
			break;

			case "calculated_value": $return_value = $element->get_summary_value_calculated();
			break;

			case "image": $return_value = $element->get_summary_value_image();
			break;

			case "submission_value": $return_value = $element->get_summary_value_formatted();
			break;
		}

		return $return_value;
	}

	public function set_form_elements($form_elements) {
		$this->form_elements = $form_elements;
	}

	public function set_form_options($form_options) {
		$this->form_options = $form_options;
	}

	public function set_form_values($form_values, $form_values_calculated) {
		$this->form_values = $form_values;
		$this->form_values_calculated = $form_values_calculated;
	}

	public function set_theme($theme = "default") {
		$this->theme = sanitize_file_name($theme);
	}

	public function set_total($total = 0) {
		$this->total = $total;
	}
}