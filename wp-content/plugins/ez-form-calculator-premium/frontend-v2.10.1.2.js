EZFC_Object = function($) {
	var _this = this;

	this.init = function() {
		// do not init twice
		if (typeof EZFC_LOADED !== "undefined") return;
		EZFC_LOADED = true;

		// form vars
		this.form_vars = [];
		// DOM form elements cached
		this.$form_elements = [];

		// form element subtotals array
		// this.subtotals[form][element_id] = subtotal_price;
		this.subtotals = [];
		// conditional once values
		this.conditional_once = [];

		// payment form id
		this.payment_form_id = 0;
		// payment methods
		this.payment_methods = ["stripe", "authorize"];

		// caching elements when using custom JS functions
		this.elements_cache = [];

		// group repeat elements
		this.group_repeat_elements = [];

		// uploaded files
		this.uploaded_files = [];

		/**
			global functions for custom calculation codes
		**/
		this.functions = {
			calculate_element: function(form_id, element_id) {
				return _this.calculate_element(form_id, element_id);
			},
			calculate_price: function(form_id) {
				var $form = $(".ezfc-form[data-id='" + form_id + "']");
				return _this.calculate_price($form);
			},
			get_element_id_by_name: function(form_id, name) {
				if (typeof _this.elements_cache[form_id][name] === "undefined") {
					_this.elements_cache[form_id][name] = $("#ezfc-form-" + form_id + " .ezfc-element[data-elementname='" + name + "']");
				}

				return _this.elements_cache[form_id][name].length ? _this.elements_cache[form_id][name].data("id") : false;
			},
			get_value_from: function(id, is_text) {
				return _this.get_value_from_element(null, id, is_text);
			},
			get_value_from_name: function(form_id, name, is_text) {
				var id = this.get_element_id_by_name(form_id, name);

				return this.get_value_from(id, is_text);
			},
			get_calculated_value_from: function(form_id, id) {
				return _this.get_calculated_element_value(form_id, id);
			},
			price_format: function(form_id, price, currency, custom_price_format, format_with_currency) {
				return _this.format_price(form_id, price, currency, custom_price_format, format_with_currency);
			}
		};
		// global functions for custom calculation codes / wrapper
		ezfc_functions = this.functions;

		// listeners for external values on change
		this.external_listeners = [];
		this.price_old_global = [];

		numeral.language("ezfc", {
			delimiters: {
				decimal:   ezfc_vars.price_format_dec_point,
				thousands: ezfc_vars.price_format_dec_thousand
			},
			abbreviations: {
				thousand: 'k',
				million: 'm',
				billion: 'b',
				trillion: 't'
			},
			ordinal: function (number) {
				var b = number % 10;
				return (~~ (number % 100 / 10) === 1) ? 'th' :
					(b === 1) ? 'st' :
					(b === 2) ? 'nd' :
					(b === 3) ? 'rd' : 'th';
			},
			currency: {
				symbol: '$'
			}
		});
		numeral.language("ezfc");

		this.defaultFormat = ezfc_vars.price_format ? ezfc_vars.price_format : "0,0[.]00";
		numeral.defaultFormat(this.defaultFormat);

		// datepicker language
		$.datepicker.setDefaults($.datepicker.regional[ezfc_vars.datepicker_language]);

		this.attach_events();
	};

	this.attach_events = function() {
		// image option listener
		$(".ezfc-element-option-image").click(function() {
			// radio option image listener
			if ($(this).hasClass("ezfc-element-radio-image")) {
				_this.radio_change_state($(this).siblings(".ezfc-element-radio-input"));
			}
			else if ($(this).hasClass("ezfc-element-checkbox-image")) {
				_this.checkbox_change_state($(this).siblings(".ezfc-element-checkbox-input"));
			}
		});
		// addon div
		$(".ezfc-addon-option").click(function() {
			$(this).siblings(".ezfc-element-option-image").click();
		});

		// init each form
		$(".ezfc-form").each(function() {
			var $form    = $(this);
			var $wrapper = $form.closest(".ezfc-wrapper");

			_this.init_form($form);
			_this.init_form_ui($form);

			if ($wrapper.find(".ezfc-form-loading-text").length) {
				$wrapper.find(".ezfc-form-loading-text").fadeOut(500, function() {
					$form.fadeIn(500, function() {
						$wrapper.removeClass("ezfc-form-loading");

						_this.form_change($form);
						_this.scroll();
					});
				});
			}

			// trigger custom event
			$(document).trigger("ezfc_forms_loaded");
		});

		// init fileupload
		$(".ezfc-element-fileupload").each(function(i, el) {
			var $parent  = $(this).closest(".ezfc-element");
			var $btn     = $parent.find(".ezfc-upload-button");
			var $list    = $parent.find(".ezfc-fileupload-files");
			var fe_id    = $parent.data("id");
			var multiple = $(this).attr("multiple") ? true : false;

			// build form data
			var $form    = $(this).closest(".ezfc-form");
			var form_id  = $form.find("input[name='id']").val();
			var ref_id   = $form.find("input[name='ref_id']").val();

			var formData = {
				action: "ezfc_frontend_fileupload",
				data: "action=upload_file&id=" + form_id + "&ref_id=" + ref_id
			};

			if (typeof $.prototype.fileupload !== "function") {
				_this.debug_message("Unable to load fileupload function.");
				return false;
			}

			$(this).fileupload({
				formData: formData,
				dataType: 'json',
				add: function (e, data) {
					$parent.find(".ezfc-bar").css("width", 0);
					$parent.find(".progress").addClass("active");
					$parent.find(".ezfc-fileupload-message").text("");

					$btn.off("click");

					// add event listener
					$btn.click(function() {
						if ($(el).val() == "") return false;

						data.files = data.originalFiles;

						data.submit();
						$($btn).attr("disabled", "disabled");

						e.preventDefault();
						return false;
					});
					
				},
				done: function (e, data) {
					$btn.removeAttr("disabled");

					if (data.result.error) {
						$parent.find(".ezfc-fileupload-message").text(data.result.error);
						$parent.find(".ezfc-bar").css("width", 0);

						return false;
					}

					// check if list for form exists
					if (typeof _this.uploaded_files[form_id] === "undefined") {
						_this.uploaded_files[form_id] = [];
					}
					// check if list for form element exists
					if (typeof _this.uploaded_files[form_id][fe_id] === "undefined") {
						_this.uploaded_files[form_id][fe_id] = [];
					}

					for (var i in data.result) {
						// add uploaded file to list
						_this.uploaded_files[form_id][fe_id].push({
							id:   data.result[i].success,
							name: data.files[i].name
						});
					}

					// clear selected file
					$(this).val("");

					if (!$(this).attr("multiple")) {
						$(this).attr("disabled", "disabled");
					}

					// clear progressbar
					$parent.find(".progress").removeClass("active");
					// add message
					$parent.find(".ezfc-fileupload-message").text(ezfc_vars.upload_success);

					// build list
					var list_out = "";
					for (var i in _this.uploaded_files[form_id][fe_id]) {
						list_out += "<li class='ezfc-fileupload-files-item' data-fileid='" + _this.uploaded_files[form_id][fe_id][i].id + "' data-refid='" + ref_id + "'>" + _this.uploaded_files[form_id][fe_id][i].name + " <span class='ezfc-fileupload-files-remove'><i class='fa fa-close'></i></span></li>";
					}
					$list.html(list_out);
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$($parent).find(".ezfc-bar").css("width", progress + "%");
				},
				replaceFileInput: false,
				url: ezfc_vars.ajaxurl
			});
		});
		// fileupload remove
		$(document).on("click", ".ezfc-fileupload-files-remove", function() {
			var $btn = $(this);
			if ($btn.hasClass("ezfc-state-removing")) return false;

			var $form    = $btn.closest(".ezfc-form");
			var form_id  = $form.find("input[name='id']").val();

			var $wrapper = $btn.closest(".ezfc-element");
			var $item    = $btn.closest(".ezfc-fileupload-files-item");
			var $msg     = $wrapper.find(".ezfc-fileupload-message");
			var $upload  = $wrapper.find(".ezfc-element-fileupload");

			var fe_id     = $wrapper.data("id");
			var file_id   = $item.data("fileid");
			var ref_id    = $item.data("refid");
			var send_data = "action=remove_uploaded_file&file_id=" + file_id + "&ref_id=" + ref_id;

			// add removing state
			$btn.addClass("ezfc-state-removing");
			// toggle icon
			$btn.find(".fa").removeClass("fa-close").addClass("fa-circle-o-notch fa-spin");

			$.ajax({
				type: "post",
				url: ezfc_vars.ajaxurl,
				data: {
					action: "ezfc_frontend_fileupload",
					data: send_data
				},
				complete: function(response) {
					var response_json;

					// check for errors
					if (typeof response === "object" && typeof response.responseText !== "undefined") {
						try {
							response_json = $.parseJSON(response.responseText);
						}
						catch(e) {}
					}

					if (!response_json) {
						$msg.text(response.upload_remove_error);
						
						// toggle icon
						$btn.find(".fa").removeClass("fa-circle-o-notch fa-spin ezfc-state-removing").addClass("fa-close");

						return false;
					}

					// re-enable multiple
					if (!$upload.attr("multiple")) {
						var files = $wrapper.find(".ezfc-fileupload-files-item").length;

						// item is still in the DOM-tree
						if (files == 1) {
							$upload.removeAttr("disabled");
						}
					}

					// remove item
					$item.fadeOut();
					$msg.text(ezfc_vars.upload_removed);

					// remove item from uploaded list
					for (var i in _this.uploaded_files[form_id][fe_id]) {
						if (_this.uploaded_files[form_id][fe_id][i].id == file_id) {
							_this.uploaded_files[form_id][fe_id].splice(i, 1);
						}
					}
				}
			});

			return false;
		});

		$(".ezfc-overview").dialog({
			autoOpen: false,
			modal: true
		});

		/**
			ui events
		**/
		$(".ezfc-form .ezfc-input-format-listener").each(function() {
			_this.input_format_listener_change($(this));
			$(this).trigger("blur");
		});

		// form has changed -> recalc price
		var input_change_func_throttle = this.throttle(function($element) {
			var form = $element.parents(".ezfc-form");
			_this.form_change(form);
		}, 100);

		$(document).on("change keyup", ".ezfc-form input, .ezfc-form select", function() {
			input_change_func_throttle($(this));
		});

		// checkbox
		$(".ezfc-element-wrapper-checkbox").change(function() {
			_this.checkbox_change($(this));
		});

		// number-slider
		$(".ezfc-slider").each(function(i, el) {
			var $target            = $(this);
			var form_id            = $(this).closest(".ezfc-form").data("id");
			var $slider_element    = $(el).siblings(".ezfc-slider-element");
			var slider_orientation = $target.hasClass("ezfc-slider-vertical") ? "vertical" : "horizontal";
			var value_normalized   = _this.normalize_value($target.val(), $target);

			var slider_object = $slider_element.slider({
				min:   $target.data("min") || 0,
				max:   $target.data("max") || 100,
				step:  $target.data("stepsslider") || 1,
				value: value_normalized || 0,
				orientation: slider_orientation,
				slide: function(ev, ui) {
					// change value before trigger
					var value = _this.normalize_value(ui.value, $target);
					$target.val(value);
					$target.trigger("change");
				},
				start: function(ev, ui) {
					$target.trigger("focus");
				},
				stop: function(ev, ui) {
					$target.trigger("blur");
				}
			});

			$(document).on("change keyup", $target, function() {
				var value = _this.normalize_value($target.val(), $target);
				slider_object.slider("value", value);
			});

			if ($target.hasClass("ezfc-pips")) {
				var pips_args = { rest: "label" };
				// check if steps is a valid number
				if (!isNaN($target.data("stepspips")) && $target.data("stepspips") != "") pips_args.step = $target.data("stepspips");

				$slider_element.slider("pips", pips_args);
				$slider_element.find(".ui-slider-pip").on("click", function() {
					var pip_val = $(this).find(".ui-slider-label").data("value");
					
					$target.val(pip_val);
					$target.trigger("change");
				});

				// check for floating pips
				if ($target.data("pipsfloat") == 1) $slider_element.slider("float");
			}
		});

		// number-spinner
		$(".ezfc-spinner").each(function(i, el) {
			var $target = $(this);

			$target.spinner({
				min:  $target.data("min") || 0,
				max:  $target.data("max") || 100,
				step: $target.data("stepsspinner") || 1,
				change: function(ev, ui) {
					$target.trigger("change");
				},
				spin: function(ev, ui) {
					// normalize
					var value = _this.normalize_value(ui.value, $target);
					// change value before trigger
					$target.val(value);
					$target.trigger("change");
				},
				start: function(ev, ui) {
					// normalize
					var value = _this.normalize_value($target.val(), $target);
					$target.val(value);
				},
				stop: function(ev, ui) {
					// normalize
					var value = _this.normalize_value($target.val(), $target);
					$target.val(value);
				}
			});
		});

		// steps
		$(".ezfc-step-button").on("click", function() {
			var form_wrapper = $(this).parents(".ezfc-form");
			var current_step = parseInt(form_wrapper.find(".ezfc-step-active").data("step"));
			var next_step    = current_step + ($(this).hasClass("ezfc-step-next") ? 1 : -1);
			var verify_step  = $(this).hasClass("ezfc-step-next") ? 1 : 0;

			_this.set_step(form_wrapper, next_step, verify_step);
			return false;
		});
		// steps indicator
		$(".ezfc-step-indicator-item-active").on("click", function() {
			var $form = $(this).closest(".ezfc-form");
			var step = parseInt($(this).data("step"));

			_this.set_step($form, step, 0);
			return false;
		});

		// payment option text switch
		$(".ezfc-element-wrapper-payment input").on("change", function() {
			var $form   = $(this).closest(".ezfc-form");
			var form_id = $form.data("id");

			// submit text will be toggled by this.price_request_toggle()
			if (_this.form_vars[form_id].price_show_request == 1 || _this.form_vars[form_id].summary_enabled) return;

			_this.set_submit_text($form);

			/*var is_paypal     = $(this).data("value")=="paypal";
			var submit_text   = is_paypal ? _this.form_vars[form_id].submit_text.paypal : _this.form_vars[form_id].submit_text.default;*/
		});

		// fixed price
		$(window).scroll(this.scroll);
		this.scroll();

		// submit button
		$(".ezfc-form").submit(function(e) {
			var $form   = $(this);
			var id      = $form.data("id");
			var $submit = $form.find(".ezfc-submit");

			if (_this.form_vars[id].hard_submit == 1) {
				return true;
			}

			_this.form_submit($form, -1, $submit.data("type"));

			e.preventDefault();
			return false;
		});
		// payment submit button
		$(".ezfc-payment-submit").click(function(e) {
			var $payment_form = $(this).closest(".ezfc-payment-form");
			var form_id       = $payment_form.data("form_id");
			var $form         = $(".ezfc-form[data-id='" + form_id + "']");

			_this.form_submit($form, -1, $(this).data("payment"));

			e.preventDefault();
			return false;
		});

		// payment cancel
		$(".ezfc-payment-cancel").click(function() {
			var $payment_form = $(this).closest(".ezfc-payment-form");
			var form_id       = $payment_form.data("form_id");
			var $form         = $("#ezfc-form-" + form_id);

			var selectors = ".ezfc-payment-dialog-modal[data-form_id='" + form_id + "']";
			selectors    += ", .ezfc-payment-form";

			$(selectors).removeClass("ezfc-payment-dialog-open");
			_this.form_submit($form, false, false, true);
			return false;
		});

		// reset button
		$(".ezfc-reset").click(function() {
			var $form = $(this).parents(".ezfc-form");
			_this.reset_form($form);

			return false;
		});

		// collapsible groups
		$(".ezfc-collapse-title-wrapper").on("click", function() {
			var $group_wrapper = $(this).closest(".ezfc-element-wrapper-group");

			_this.toggle_group($group_wrapper);
		});

		// credit card number formatter
		$(".ezfc-cc-number-formatter").on("change keyup", function() {
			var value = $(this).val();
			value = value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
			$(this).val(value);
		});

		// repeatable group
		$(".ezfc-group-repeat").on("click", function() {
			_this.group_repeat($(this).data("group_repeat_id"));

			return false;
		});
	};

	/**
		init form UI
	**/
	this.init_form_ui = function(form_dom, build_groups) {
		if (typeof build_groups === "undefined") build_groups = true;

		var $form    = $(form_dom);
		var form_id  = $form.data("id");
		
		// datepicker
		$form.find(".ezfc-element-datepicker").each(function() {
			var $element = $(this);

			var el_settings = {};
			if ($element.data("settings")) {
				el_settings = $element.data("settings");
			}

			// get available days
			var available_days = [0, 1, 2, 3, 4, 5, 6];
			if (el_settings.available_days.length) {
				available_days = el_settings.available_days.split(",");
			}
			// convert to int
			available_days = available_days.map(function(d) {
				return parseInt(d);
			});

			var blocked_days = [];
			if (el_settings.blocked_days.length) {
				blocked_days = el_settings.blocked_days.split(",");
			}

			$element.datepicker({
				dateFormat:     _this.form_vars[form_id].datepicker_format,
				minDate:        el_settings.minDate ? el_settings.minDate : "",
				maxDate:        el_settings.maxDate ? el_settings.maxDate : "",
				numberOfMonths: el_settings.numberOfMonths ? parseInt(el_settings.numberOfMonths) : 1,
				showAnim:       el_settings.showAnim ? el_settings.showAnim : "fadeIn",
				showWeek:       el_settings.showWeek=="1" ? el_settings.showWeek : false,
				firstDay:       el_settings.firstDay ? el_settings.firstDay : false,
				beforeShowDay: function(date) {
					return _this.check_datepicker_days(date, available_days, blocked_days);
				}
			});

			// set today's date
			if ($element.val() == "__today__") {
				$element.datepicker("setDate", new Date());
			}
		});

		// daterange
		$form.find(".ezfc-element-daterange").each(function() {
			var $element = $(this);

			var el_settings = $element.closest(".ezfc-element-daterange-container").data("settings");

			// get available days
			var available_days = [0, 1, 2, 3, 4, 5, 6];
			if (el_settings.available_days.length) {
				available_days = el_settings.available_days.split(",");
			}
			// convert to int
			available_days = available_days.map(function(d) {
				return parseInt(d);
			});

			var blocked_days = [];
			if (el_settings.blocked_days.length) {
				blocked_days = el_settings.blocked_days.split(",");
			}

			// from
			if ($element.hasClass("ezfc-element-daterange-from")) {
				$element.datepicker({
					dateFormat:     _this.form_vars[form_id].datepicker_format,
					minDate:        $element.data("mindate"),
					maxDate:        $element.data("maxdate"),
					numberOfMonths: el_settings.numberOfMonths ? parseInt(el_settings.numberOfMonths) : 1,
					showAnim:       el_settings.showAnim ? el_settings.showAnim : "fadeIn",
					showWeek:       el_settings.showWeek=="1" ? el_settings.showWeek : false,
					firstDay:       el_settings.firstDay ? el_settings.firstDay : false,
					onSelect: function(selectedDate) {
						var minDays = $element.data("mindays") || 0;
						var maxDays = $element.data("maxdays") || 0;

						var minDate = $.datepicker.parseDate(_this.form_vars[form_id].datepicker_format, selectedDate);
						minDate.setDate(minDate.getDate() + minDays);
						var maxDate = $.datepicker.parseDate(_this.form_vars[form_id].datepicker_format, selectedDate);
						maxDate.setDate(maxDate.getDate() + maxDays);

						$element.siblings(".ezfc-element-daterange-to").datepicker("option", "minDate", minDate);
						if (maxDays != 0) $element.siblings(".ezfc-element-daterange-to").datepicker("option", "maxDate", maxDate);
						$element.trigger("change");
					},
					beforeShowDay: function(date) {
						return _this.check_datepicker_days(date, available_days, blocked_days);
					}
				});

				// set today's date
				if ($element.val() == "__today__") {
					$element.datepicker("setDate", new Date());
				}
			}
			// to
			else {
				$element.datepicker({
					dateFormat:     _this.form_vars[form_id].datepicker_format,
					minDate:        $element.data("mindate"),
					maxDate:        $element.data("maxdate"),
					numberOfMonths: el_settings.numberOfMonths ? parseInt(el_settings.numberOfMonths) : 1,
					showAnim:       el_settings.showAnim ? el_settings.showAnim : "fadeIn",
					showWeek:       el_settings.showWeek=="1" ? el_settings.showWeek : false,
					firstDay:       el_settings.firstDay ? el_settings.firstDay : false,
					onSelect: function(selectedDate) {
						$element.trigger("change");
					},
					beforeShowDay: function(date) {
						return _this.check_datepicker_days(date, available_days, blocked_days);
					}
				});

				// set today's date
				if ($element.val() == "__today__") {
					$element.datepicker("setDate", new Date());
				}
			}
		});

		// timepicker
		$form.find(".ezfc-element-timepicker").each(function() {
			var $element = $(this);

			var el_settings = {};
			if ($element.data("settings")) {
				el_settings = $element.data("settings");
			}

			$element.timepicker({
				minTime:    el_settings.minTime ? el_settings.minTime : null,
				maxTime:    el_settings.maxTime ? el_settings.maxTime : null,
				step:       el_settings.steps ? el_settings.steps : 30,
				timeFormat: el_settings.format ? el_settings.format : _this.form_vars[form_id].timepicker_format
			});
		});

		// colorpicker
		$form.find(".ezfc-element-colorpicker").each(function() {
			var $element = $(this);
			var input    = $element.parents(".ezfc-element").find(".ezfc-element-colorpicker-input");

			var colorpicker = $element.colorpicker({
				container: $element
			}).on("changeColor.colorpicker", function(ev) {
				$element.css("background-color", ev.color.toHex());
				input.val(ev.color.toHex());
			});

			$(input).on("click focus", function() {
				colorpicker.colorpicker("show");
			}).on("change", function() {
				colorpicker.colorpicker("setValue", $form.val());
			});
		});

		// if steps are used, move the submission button + summary table to the last step
		var steps = $form.find(".ezfc-step");
		if (steps.length > 0) {
			var last_step = steps.last();
			
			$form.find(".ezfc-summary-wrapper").appendTo(last_step);
			$form.find(".ezfc-submit-wrapper").appendTo(last_step).addClass("ezfc-submit-step");

			// prevent enter step in last step
			this.prevent_enter_step_listener(last_step.find("input"), $form);
		}

		// put elements into groups
		$(".ezfc-custom-element[data-group]").each(function() {
			var $element       = $(this);
			var fe_id          = $element.data("id");
			var group_id       = $element.data("group");
			var $group_element = $("#ezfc_element-" + group_id);

			if ($group_element.data("element") != "group") return;

			// append element to group
			if ($group_element.length > 0) {
				var $group_element_wrapper = $group_element.find("> .ezfc-group-elements");
				$element.appendTo($group_element_wrapper);
			}
		});

		/*// change input names for repeatable groups
		$.each(ezfc_vars.element_vars[form_id], function(fe_id, element_vars) {
			if (typeof element_vars.repeatable === "undefined" || !element_vars.repeatable) return;

			$("#ezfc_element-" + fe_id).find("[name]").each(function() {
				var new_name = $(this).attr("name") + "[]";
				$(this).attr("name", new_name);
			});
		});*/

		_this.init_form(form_dom);
	};

	/**
		init form
	**/
	this.init_form = function(form_dom) {
		var $form    = $(form_dom);
		var form_id  = $form.data("id");
		
		this.form_vars[form_id] = $form.data("vars");
		if (typeof this.form_vars[form_id] !== "object") {
			this.form_vars[form_id] = $.parseJSON($form.data("vars"));
		}

		// cache form elements
		_this.$form_elements[form_id] = [];
		for (var i in _this.form_vars[form_id].form_elements_order) {
			_this.$form_elements[form_id].push($("#ezfc_element-" + _this.form_vars[form_id].form_elements_order[i]));
		}

		// subtotals array
		this.subtotals[form_id] = [];
		// conditional once values
		this.conditional_once[form_id] = [];

		// init listener for each form
		this.external_listeners[form_id] = [];

		// elements cache
		this.elements_cache[form_id] = [];

		// set request price text
		if (this.form_vars[form_id].price_show_request == 1) {
			this.price_request_toggle(form_id, false);
		}

		// set price
		this.set_price($form);
		// set submit text
		this.set_submit_text($form);

		// hide woocommerce price+button
		if (this.form_vars[form_id].use_woocommerce) {
			var elements_to_hide = ".woocommerce form.cart, .woocommerce .price";

			$(elements_to_hide).hide();
		}

		// step indicator start added by 1 so it becomes more readable
		var step_indicator_start = parseInt(this.form_vars[form_id].step_indicator_start) + 1;
		if (step_indicator_start > 1) {
			$form.find(".ezfc-step-indicator").hide();
		}

		// init debug info
		if (ezfc_vars.debug_mode == 2 && !$("#ezfc-show-all-elements").length) {
			$form.append("<button id='ezfc-show-all-elements'>Show/hide elements</button>");

			$("#ezfc-show-all-elements").click(function() {
				if ($form.hasClass("ezfc-debug-visible")) {
					$form.removeClass("ezfc-debug-visible");
					$form.find(".ezfc-tmp-visible").removeClass("ezfc-tmp-visible").hide();
				}
				else {
					$form.addClass("ezfc-debug-visible");
					$form.find(".ezfc-hidden").addClass("ezfc-tmp-visible").show().css("display", "inline-block");
				}

				return false;
			});
		}

		// stripe
		if (this.form_vars[form_id].use_stripe && typeof Stripe !== "undefined") {
			Stripe.setPublishableKey(ezfc_vars.stripe.publishable_key);
		}

		// prevent enter key to submit form
		this.prevent_enter_step_listener(":input:not(textarea):not([type=submit])", $form);

		// populate html placeholders
		this.populate_html_placeholders($form);

		// trigger custom event
		$(document).trigger("ezfc_form_init", form_id);
	};

	// form has changed
	this.form_change = function(form) {
		var form_id = $(form).data("id");

		// clear hidden values
		this.clear_hidden_values(form);

		// form has changed -> reset price + summary
		this.form_vars[form_id].price_requested = 0;
		this.form_vars[form_id].summary_shown = 0;

		$(form).find(".ezfc-summary-wrapper").fadeOut();
		
		// price request
		if (this.form_vars[form_id].price_show_request == 1) {
			this.price_request_toggle(form_id, false);

			return false;
		}
		
		// remove debug info
		this.remove_debug_info();

		this.set_price(form);
		this.set_submit_text(form);

		// populate html placeholder data
		this.populate_html_placeholders(form);
	};

	/**
		form submitted
	**/
	this.form_submit = function(form, step, submit_type, cancel) {
		var $form         = $(form);
		var $form_wrapper = $form.closest(".ezfc-wrapper");
		var id            = $form.data("id");

		// if this flag is true, the ajax request will be cancelled
		var form_error = false;

		// show submit icon
		$form.find(".ezfc-submit-icon").addClass("ezfc-submit-icon-show");

		// set spinner icon to submit field
		var submit_icon    = $form.find(".ezfc-submit-icon, .ezfc-step-submit-icon");
		var submit_element = $form.find("input[type='submit']");

		// price request
		if (this.form_vars[id].price_show_request == 1) {
			this.set_price(form);
		}

		// cancel submit
		if (cancel) {
			_this.submit_cancel($form);
			return false;
		}

		// payment dialog
		if ($.inArray(submit_type, _this.payment_methods) !== -1) {
			var $payment_form = $("#ezfc-" + submit_type + "-form-" + id);

			// fill placeholders with total price
			var price_html = this.format_price(id, this.price_old_global[id], false, false, true);
			$payment_form.find(".ezfc-payment-price").text(price_html);

			// show cc details dialog
			if (!_this.form_vars[id].payment_info_shown[submit_type]) {
				_this.form_vars[id].payment_info_shown[submit_type] = 1;
				// open dialog + modal
				$("#ezfc-" + submit_type + "-form-" + id + ", #ezfc-" + submit_type + "-form-modal-" + id).addClass("ezfc-payment-dialog-open");
			}
			// create request token
			else {
				_this.payment_form_id = id;

				// disable action buttons
				$payment_form.find(".ezfc-payment-submit, .ezfc-payment-cancel").prop("disabled", true);

				if (submit_type == "stripe") {
					// create token
					Stripe.card.createToken($payment_form, this.stripe_response_handler);
				}
				else if (submit_type == "authorize") {
					if (typeof Accept === "undefined") {
						_this.debug_message("AcceptJS wasn't loaded.");
						return false;
					}

					this.authorize_create_token(id);
				}
			}

			submit_icon.fadeOut();
			return false;
		}

		// show loading icon, disable submit button
		submit_icon.fadeIn();
		submit_element.prop("disabled", true);

		// clear hidden elements (due to conditional logic)
		$form.find(".ezfc-custom-hidden:not(.ezfc-element-wrapper-fileupload):not(.ezfc-element-wrapper-group)").each(function() {
			// empty radio buttons --> select first element to submit __hidden__ data
			var radio_empty = $(this).find(".ezfc-element-radio:not(:has(:radio:checked))");
			if (radio_empty.length) {
				$(radio_empty).first().find("input").prop("checked", true);
			}
			
			// todo: re-add disabled attribute after price request / summary
			$(this).find("input, :selected").val("__HIDDEN__").removeAttr("disabled").addClass("ezfc-has-hidden-placeholder");
		});

		// add special hidden value for conditionally hidden elements
		$form.find(".ezfc-element[data-calculate_activated='0']").each(function() {
			$(this).find("input, :selected").val("__HIDDEN__");
		});

		// check for min/max selectable
		$form.find(".ezfc-element-wrapper-checkbox").each(function() {
			// check for hidden placeholder
			// TODO TEST
			if ($(this).find(".ezfc-has-hidden-placeholder").length) return false;

			var $element     = $(this);
			var min_selected = parseInt($element.attr("data-min_selectable"));
			var max_selected = parseInt($element.attr("data-max_selectable"));
			var selected     = $element.find(":checked").length;

			// check min selected
			if (min_selected > 0 && selected < min_selected) {
				form_error = true;
				_this.show_tip($element, "#" + $element.attr("id"), 0, _this.sprintf(_this.form_vars[id].selectable_min_error, min_selected));
				return false;
			}
			// check max selected
			if (max_selected > 0 && selected > max_selected) {
				form_error = true;
				_this.show_tip($element, "#" + $element.attr("id"), 0, _this.sprintf(_this.form_vars[id].selectable_max_error, max_selected));
				return false;
			}
		});

		// store values in tmp array
		var values_before_submission = [];
		$form.find("[data-is_number='1'] input").each(function() {
			// do not mess with condtionally hidden elements
			if (this.value == "__HIDDEN__") return;

			values_before_submission.push({
				element: $(this),
				value: this.value
			});

			this.value = this.value.replace(/[^0-9\.,-]/g, "");
		});

		var data = $form.serialize();

		// restore values
		$.each(values_before_submission, function(i, obj) {
			$(obj.element).val(obj.value);
		});

		if (form_error) {
			submit_icon.fadeOut();
			submit_element.removeAttr("disabled");
			return false;
		}

		// url
		data += "&url=" + encodeURI(window.location.href);

		// request price for the first time
		if (this.form_vars[id].price_requested == 0) {
			data += "&price_requested=1";
		}

		// summary
		if (this.form_vars[id].summary_shown == 0) {
			data += "&summary=1";
		}

		// next/previous step
		if (step != -1) {
			data += "&step=" + step;
		}

		// preview
		if (this.form_vars[id].preview_form) {
			data += "&preview_form=" + this.form_vars[id].preview_form;
		}

		data += "&generated_price=" + this.price_old_global[id];

		this.call_hook("ezfc_before_submission", {
			data: data,
			form: $form,
			form_vars: this.form_vars[id],
			id: id
		});

		$.ajax({
			type: "post",
			url: ezfc_vars.ajaxurl,
			data: {
				action: "ezfc_frontend",
				data: data
			},
			success: function(response) {
				$(".ezfc-submit-icon").removeClass("ezfc-submit-icon-show");

				submit_element.removeAttr("disabled");
				submit_icon.fadeOut();

				_this.debug_message(response);

				try {
					response = $.parseJSON(response);
				}
				catch (e) {
					response = false;
					_this.debug_message(e);
				}

				if (!response) {
					$form.find(".ezfc-message").text("Something went wrong. :(");
					_this.recaptcha_reload();
					_this.reset_disabled_fields(form, true);
						
					return false;
				}

				// error occurred -> invalid form fields
				if (typeof response.error !== "undefined") {
					_this.reset_disabled_fields(form, true);

					if (response.id) {
						// error tip (if the form uses steps, do not show this if all fields are valid up until this step)
						var show_error_tip = true;
						// error tip data
						var el_target = "#ezfc_element-" + response.id;
						var el_tip    = $(el_target).find(".ezfc-element").first();
						var tip       = null;

						// check if form uses steps
						var use_steps = $form.find(".ezfc-step-active").length > 0 ? true : false;
						if (use_steps) {
							var error_step = parseInt($(el_target).parents(".ezfc-step").data("step"));
							
							// if invalid field is not on the current step, do not show the tip. also, do not show the tip when submitting the form (step = -1)
							if (error_step != step && step != -1) {
								show_error_tip = false;
								_this.set_step(form, step + 1);
							}
						}

						if (show_error_tip) {
							if (!el_tip.length) {
								el_tip = $(el_target);
							}

							var tip_delay = use_steps ? 1000 : 400;
							var payment_dialog_open = $form_wrapper.find(".ezfc-payment-dialog-open").length > 0;

							// element is visible
							if (el_tip.is(":visible")) {
								_this.show_tip(el_tip, el_target, tip_delay, response.error);

								// payment dialog is opened
								if (payment_dialog_open) {
									_this.set_message(id, response.error + " (#" + response.id + ")");
								}
							}
							else {
								var $el_tip_parent_groups = el_tip.parents(".ezfc-element-wrapper-group");

								// element is hidden in a group
								if ($el_tip_parent_groups.length) {
									$el_tip_parent_groups.each(function(gi, group_el) {
										_this.toggle_group($(group_el), true);
									});

									_this.show_tip(el_tip, el_target, tip_delay, response.error);
								}
								// element is still hidden
								else {
									_this.set_message(id, response.error + " (#" + response.id + ")");
								}
							}

							// auto hide tooltip
							if (typeof ezfc_vars.required_text_auto_hide !== "undefined") {
								var required_text_auto_hide = parseFloat(ezfc_vars.required_text_auto_hide) * 1000;

								if (required_text_auto_hide > 0) {
									setTimeout(function() {
										if (tip) tip.hide();
									}, required_text_auto_hide);
								}
							}

							if (_this.form_vars[id].disable_error_scroll == 0) {
								_this.scroll_to(el_target);
							}
						}
					}
					else {
						_this.set_message(id, response.error);
					}

					_this.recaptcha_reload();

					return false;
				}
				// next step
				else if (response.step_valid) {
					_this.reset_disabled_fields(form);
					_this.set_step(form, step + 1);

					return false;
				}
				// summary
				else if (response.summary) {
					$form.find(".ezfc-summary-wrapper").fadeIn().find(".ezfc-summary").html(response.summary);
					_this.form_vars[id].summary_shown = 1;

					_this.reset_disabled_fields(form);

					return false;
				}

				// prevent spam
				_this.recaptcha_reload();

				// submit paypal form
				if (response.paypal) {
					// disable submit button again to prevent doubleclicking
					submit_element.attr("disabled", "disabled");
					// redirect to paypal express checkout url
					window.location.href = response.paypal;
				}
				else {
					// price request
					if (response.price_requested || response.price_requested === 0) {
						_this.price_request_toggle(id, true, response.price_requested);
						return false;
					}

					/**
						submission successful
					**/
					var hook_vars = {
						data: data,
						form: $form,
						form_vars: _this.form_vars[id],
						id: id,
						price: price,
						response: response
					};
					// hook
					_this.call_hook("ezfc_submission_success", hook_vars);
					// call custom js function
					if (_this.form_vars[id].submission_js_func && typeof window[_this.form_vars[id].submission_js_func] === "function") {
						window[_this.form_vars[id].submission_js_func](hook_vars);
					}

					// hide payment dialog(s)
					$form_wrapper.find(".ezfc-payment-dialog, .ezfc-payment-dialog-modal").removeClass("ezfc-payment-dialog-open");

					// add success text
					var $success_text = $(".ezfc-success-text[data-id='" + id + "']");
					$success_text.html(response.success);

					// reset form after submission
					if (_this.form_vars[id].reset_after_submission == 1) {
						_this.reset_form(form);

						$success_text.fadeIn().delay(7500).fadeOut();
						return;
					}

					// hide all forms
					if (_this.form_vars[id].hide_all_forms == 1) {
						$(".ezfc-form, .ezfc-required-notification").fadeOut();
					}
					else {
						$form.find(".ezfc-required-notification").fadeOut();

						if (_this.form_vars[id].show_success_text == 1) {
							$form.fadeOut();
						}
					}

					// show success text
					if (_this.form_vars[id].show_success_text == 1) {
						// scroll to success message
						if (_this.form_vars[id].scroll_to_success_message == 1) {
							$success_text.fadeIn(400, function() {
								_this.scroll_to($success_text, -200);
							});
						}
						else {
							$success_text.fadeIn();
						}
					}

					// update mini cart
					if (response.woo_update_cart && response.woo_cart_html && ezfc_vars.woocommerce_update_cart_selector.length > 0) {
						$(ezfc_vars.woocommerce_update_cart_selector).html(response.woo_cart_html);
					}

					// redirect the user
					if (typeof _this.form_vars[id].redirect_url !== "undefined" && _this.form_vars[id].redirect_url.length > 0) {
						var redirect_form_vars = "";

						if (_this.form_vars[id].redirect_forward_values == 1) {
							redirect_form_vars = $form.serialize();
							redirect_form_vars += "&total=" + _this.price_old_global[id];
							redirect_form_vars += "&total_f=" + _this.functions.price_format(id, _this.price_old_global[id]);
						}

						var href_separator = _this.form_vars[id].redirect_url.indexOf("?") == -1 ? "?" : "&";
						window.location.href = _this.form_vars[id].redirect_url + href_separator + redirect_form_vars;
					}
					// refresh the page
					else if (typeof _this.form_vars[id].refresh_page_after_submission !== "undefined" && _this.form_vars[id].refresh_page_after_submission == 1) {
						var redirect_timer = Math.max(0, Math.abs(parseInt(_this.form_vars[id].redirect_timer)));

						setTimeout(function() {
							window.location.reload();
						}, redirect_timer * 1000);
					}
				}
			}
		});
	};

	/**
		external values
	**/
	this.calculate_get_external_values = function(form, form_id, el_object, el_type) {
		var value_external_element = el_object.data("value_external");
		var value_external_listen  = el_object.data("value_external_listen");

		// only do it once if listen is disabled
		if (this.external_listeners[form_id][value_external_element] && !value_external_listen) return;

		if (value_external_element && $(value_external_element).length > 0) {
			// get external value
			var value_external;

			if ($(value_external_element).is("input[type='radio']")) {
				value_external = $(value_external_element).find(":checked").val();
			}
			else if ($(value_external_element).is("input, input[type='text'], textarea")) {
				value_external = $(value_external_element).val();
			}
			else if ($(value_external_element).is("select")) {
				value_external = $(value_external_element).find(":selected").text();
			}
			else {
				value_external = $(value_external_element).text();
			}

			// set external value
			if (el_type == "input" || el_type == "numbers" || el_type == "subtotal") {
				el_object.find("input").val(value_external);
			}
			else if (el_type == "dropdown") {
				el_object.find(":selected").removeAttr("selected");
				el_object.find("option[value='" + value_external + "']").attr("selected", "selected");
			}
			else if (el_type == "radio") {
				el_object.find(":checked").removeAttr("checked");
				el_object.find("input[value='" + value_external + "']").attr("checked", "checked");
			}
			else if (el_type == "checkbox") {
				el_object.find(":checked").removeAttr("checked");
				el_object.find("input[value='" + value_external + "']").attr("checked", "checked");
			}
			else if (el_type == "textfield") {
				el_object.find("textarea").val(value_external);
			}

			// set event listener
			if (!this.external_listeners[form_id][value_external_element]) {
				this.external_listeners[form_id][value_external_element] = 1;

				$(value_external_element).on("change keyup", function() {
					_this.set_price($(form));
				});
			}
		}
	};


	/**
		conditionals
	**/
	this.calculate_conditionals = function(form, form_id, el_object, el_type) {
		var el_id = el_object.data("id");

		// check if conditionals exist
		if (!el_id || typeof ezfc_vars.element_vars[form_id][el_id].conditional === "undefined") return;
		// conditional rows
		var cond = ezfc_vars.element_vars[form_id][el_id].conditional;

		// value of this element (but beware of is_number due to text values)
		var is_number = el_object.data("is_number");
		var el_value  = this.get_value_from_element(el_object, null, !is_number);
		var el_factor = 1;

		// go through all conditionals
		$.each(cond.action, function(ic, action) {
			if (!action || action == 0) return;

			// get conditional target element
			var cond_target;
			if (cond.target[ic] == "submit_button") {
				cond_target = $(form).find(".ezfc-submit");
			}
			else if (cond.target[ic] == "price") {
				cond_target = $(form).find(".ezfc-price-wrapper-element");
			}
			else {
				cond_target = $("#ezfc_element-" + cond.target[ic]);
			}

			// no target element found
			if (cond_target.length < 1 && cond.redirects.length < 1) return;

			// check if raw value should be used
			if (cond.use_factor[ic] == 1) {
				el_factor = parseFloat(el_factor);
				if (!isNaN(el_factor)) {
					el_value *= el_factor;
				}
			}

			// chaining
			var conditional_chain = [ { operator: cond.operator[ic], value: cond.values[ic], compare_target: "" } ];

			if (typeof cond.chain !== "undefined" && typeof cond.chain[ic] !== "undefined" && typeof cond.chain[ic].operator) {
				$.each(cond.chain[ic].operator, function(cn, operator_chain) {
					conditional_chain.push({ operator: cond.chain[ic].operator[cn], value: cond.chain[ic].value[cn], compare_target: cond.chain[ic].compare_target[cn] });
				});
			}

			/**
				check all conditional chains
			**/
			var do_action       = false;
			var do_action_count = 0; // for special checks

			$.each(conditional_chain, function(chain_index, chain_row) {
				var do_action_index    = false; // TODO
				var cond_custom_value  = chain_row.value;
				var cond_value_min_max = [];

				var tmp_compare_value = el_value;
				// get value from compare target
				if (chain_row.compare_target != 0 && chain_row.compare_target != "") {
					tmp_compare_value = _this.get_value_from_element(false, chain_row.compare_target);
				}

				// override compare value with element's value
				if (chain_row.value == "__self__") {
					chain_row.value = el_value;
				}

				// only parse floats if value is a number
				var parse_exceptions = ["in", "not_in", "selected", "not_selected", "selected_index", "not_selected_index", "selected_id", "not_selected_id", "selected_count", "not_selected_count"];
				if (is_number == 1 && $.inArray(chain_row.operator, parse_exceptions) === -1) {
					cond_custom_value = parseFloat(chain_row.value);
				}
				else {
					cond_custom_value = chain_row.value;
				}

				// check for between-operator exception
				if (chain_row.operator == "between" || chain_row.operator == "not_between") {
					cond_value_min_max = chain_row.value.split(":");
					if (cond_value_min_max.length > 1) {
						cond_value_min_max[0] = parseFloat(cond_value_min_max[0]);
						cond_value_min_max[1] = parseFloat(cond_value_min_max[1]);
					}
				}

				// check if conditional is true - separate text and number elements
				if (el_type == "input") {
					do_action_index = cond_custom_value.toLowerCase()==cond_target.val().toLowerCase();
				}
				else {
					switch (chain_row.operator) {
						case "gr": do_action_index = tmp_compare_value > cond_custom_value;
						break;
						case "gre": do_action_index = tmp_compare_value >= cond_custom_value;
						break;

						case "less": do_action_index = tmp_compare_value < cond_custom_value;
						break;
						case "lesse": do_action_index = tmp_compare_value <= cond_custom_value;
						break;

						case "equals": do_action_index = tmp_compare_value == cond_custom_value;
						break;

						case "between":
							if (cond_value_min_max.length < 2) {
								do_action_index = false;
							}
							else {
								do_action_index = (tmp_compare_value >= cond_value_min_max[0] && tmp_compare_value <= cond_value_min_max[1]);
							}
						break;

						case "not_between":
							if (cond_value_min_max.length < 2) {
								do_action_index = false;
							}
							else {
								do_action_index = (tmp_compare_value < cond_value_min_max[0] || tmp_compare_value > cond_value_min_max[1]);
							}
						break;

						case "not":
							if (cond_value_min_max.length < 2) {
								do_action_index = tmp_compare_value != cond_custom_value;
							}
							else {
								do_action_index = (tmp_compare_value < cond_value_min_max[0] && tmp_compare_value > cond_value_min_max[1]);
							}
						break;

						case "hidden": do_action_index = !_this.element_is_visible(form_id, el_id);
						break;

						case "visible": do_action_index = _this.element_is_visible(form_id, el_id);
						break;

						case "mod0": do_action_index = tmp_compare_value > 0 && (tmp_compare_value % cond_custom_value) == 0;
						break;
						case "mod1": do_action_index = tmp_compare_value > 0 && (tmp_compare_value % cond_custom_value) != 0;
						break;

						case "bit_and": do_action_index = tmp_compare_value & cond_custom_value;
						break;

						case "bit_or": do_action_index = tmp_compare_value | cond_custom_value;
						break;

						case "empty":
							if (typeof tmp_compare_value === "undefined") {
								do_action_index = true;
							}
							if (typeof tmp_compare_value === "number") {
								do_action_index = isNaN(tmp_compare_value);
							}
							else {
								do_action_index = tmp_compare_value.length < 1;
							}
						break;

						case "notempty":
							if (typeof tmp_compare_value === "undefined") {
								do_action_index = false;
							}
							else if (typeof tmp_compare_value === "number") {
								do_action_index = !isNaN(tmp_compare_value);
							}
							else {
								do_action_index = tmp_compare_value.length > 0;
							}
						break;

						case "in":
							if (typeof tmp_compare_value === "undefined") {
								do_action_index = false;
							}
							else {
								var in_values = cond_custom_value.split("|");

								do_action_index = false;
								for (var i in in_values) {
									if (tmp_compare_value == in_values[i]) {
										do_action_index = true;
										return;
									}
								}
							}
						break;

						case "not_in":
							if (typeof tmp_compare_value === "undefined") {
								do_action_index = false;
							}
							else {
								var in_values = cond_custom_value.split("|");

								do_action_index = true;
								for (var i in in_values) {
									if (tmp_compare_value == in_values[i]) {
										do_action_index = false;
										return;
									}
								}
							}
						break;

						case "once":
							do_action_index = true;

							if (typeof _this.conditional_once[form_id][el_id] === "undefined") {
								_this.conditional_once[form_id][el_id] = [];
							}
							if (typeof _this.conditional_once[form_id][el_id][ic] === "undefined") {
								_this.conditional_once[form_id][el_id][ic] = [];
							}

							if (typeof _this.conditional_once[form_id][el_id][ic][chain_index] === "undefined") {
								_this.conditional_once[form_id][el_id][ic][chain_index] = 1;
							}
							else {
								do_action_index = false;
							}
						break;

						case "selected":
						case "selected_index":
						case "selected_id":
						case "selected_count":
						case "selected_count_gt":
						case "selected_count_lt":
							do_action_index = false;

							// get array value
							var tmp_return_value = "value";
							if (chain_row.operator == "selected_index") tmp_return_value = "index";
							else if (chain_row.operator == "selected_id") tmp_return_value = "option_id";
							else if (chain_row.operator == "selected_count" || chain_row.operator == "selected_count_gt" || chain_row.operator == "selected_count_lt") tmp_return_value = "count";

							var tmp_value;
							if (chain_row.compare_target != 0 && chain_row.compare_target != "") {
								tmp_value = _this.get_value_from_element(false, chain_row.compare_target, true, false, { return_array: true, return_value: tmp_return_value });
							}
							else {
								tmp_value = _this.get_value_from_element(el_object, null, true, false, { return_array: true, return_value: tmp_return_value });
							}

							if (typeof tmp_value === "object") {
								var in_values = cond_custom_value.split("|");

								for (var i in in_values) {
									cmp_value = in_values[i];

									for (var t in tmp_value) {
										if (tmp_value[t] == cmp_value) {
											do_action_index = true;
										}
									}
								}
							}
							// other compare method (sum, count etc.)
							else {
								if (chain_row.operator == "selected_count") do_action_index = tmp_value == cond_custom_value;
								else if (chain_row.operator == "selected_count_gt") do_action_index = tmp_value > cond_custom_value;
								else if (chain_row.operator == "selected_count_lt") do_action_index = tmp_value < cond_custom_value;
								else do_action_index = tmp_value == cond_custom_value;
							}
						break;

						case "not_selected":
						case "not_selected_index":
						case "not_selected_id":
						case "not_selected_count":
							do_action_index = true;

							// get array value
							var tmp_return_value = "value";
							if (chain_row.operator == "not_selected_index") tmp_return_value = "index";
							else if (chain_row.operator == "not_selected_id") tmp_return_value = "option_id";
							else if (chain_row.operator == "not_selected_count") tmp_return_value = "count";

							var tmp_value;
							if (chain_row.compare_target != 0 && chain_row.compare_target != "") {
								tmp_value = _this.get_value_from_element(false, chain_row.compare_target, true, false, { return_array: true, return_value: tmp_return_value });
							}
							else {
								tmp_value = _this.get_value_from_element(el_object, null, true, false, { return_array: true, return_value: tmp_return_value });
							}

							if (typeof tmp_value === "object") {
								var in_values = cond_custom_value.split("|");

								for (var i in in_values) {
									cmp_value = in_values[i];

									for (var t in tmp_value) {
										if (tmp_value[t] == cmp_value) {
											do_action_index = false;
											return;
										}
									}
								}
							}
							// other compare method (sum, count etc.)
							else {
								do_action_index = tmp_value != cond_custom_value;
							}
						break;

						case "focus":
							do_action_index = el_object.find("input").is(":focus");
						break;
						case "blur":
							do_action_index = !el_object.find("input").is(":focus");
						break;

						case "always":
							do_action_index = true;
						break;

						default:
							do_action_index = false;
						break;						
					}
				}

				// at least one condition needs to be true (i.e. row OR operator)
				if (typeof cond.row_operator[ic] !== "undefined" && cond.row_operator[ic] == 1) {
					if (do_action_index) {
						do_action_count++;
						do_action = true;
						return false;
					}
				}
				// all conditions need to be true (i.e. row AND operator)
				else {
					if (!do_action_index) return;

					do_action_count++;
				}
			});

			// check if all conditions are true
			if (do_action_count > 0 && do_action_count == conditional_chain.length) {
				do_action = true;
			}

			// conditional actions
			var js_action, js_counter_action;
			// when cond_notoggle_element is true, the opposite action will not be executed
			var cond_notoggle_element = cond.notoggle[ic];
			// target element type
			var cond_target_type = cond_target.data("element");
			// target value
			var tmp_custom_value = cond.target_value[ic];
			// replace __self__ with own value
			tmp_custom_value = tmp_custom_value.replace("__self__", el_value);

			// set cond_target to all direct child elements when it's a group
			if (cond_target_type == "group") {
				cond_target.push($(cond_target).find("> .ezfc-custom-element"));
			}

			// set values
			if (action == "set" && do_action) {
				// check first char for inline math
				var first_char = cond.target_value[ic].toString().charAt(0);
				// inline math for conditional set action
				if ($.inArray(first_char, ["+", "-", "*", "/"]) !== -1) {
					var value_sanitized  = tmp_custom_value.substring(1);
					var target_value     = _this.get_value_from_element(cond_target, null, false, true, { return_value: "base" });
					var target_value_new;

					// set sanitized value to this value
					if (value_sanitized == "__self__") {
						value_sanitized = el_value;
					}
					
					value_sanitized = parseFloat(value_sanitized);

					if (first_char == "+")      target_value_new = target_value + value_sanitized;
					else if (first_char == "-") target_value_new = target_value - value_sanitized;
					else if (first_char == "*") target_value_new = target_value * value_sanitized;
					else if (first_char == "/") target_value_new = target_value / value_sanitized;

					tmp_custom_value = target_value_new;
				}

				if (cond_target_type == "input" || cond_target_type == "hidden" || cond_target_type == "numbers" || cond_target_type == "subtotal" || cond_target_type == "set") {
					cond_target.find("input").val(tmp_custom_value).trigger("blur");
				}
				else if (cond_target_type == "dropdown") {
					cond_target.find(":selected").removeAttr("selected");
					cond_target.find("option[data-value='" + tmp_custom_value + "']").prop("selected", "selected");
				}
				else if (cond_target_type == "radio") {
					_this.radio_change_state(cond_target.find("input[data-value='" + tmp_custom_value + "']"), true);
				}
				else if (cond_target_type == "checkbox") {
					_this.checkbox_change_state(cond_target.find("input[data-value='" + tmp_custom_value + "']"), true);
				}
				else {
					cond_target.text(tmp_custom_value);
				}
			}
			// set factor
			else if (action == "set_factor" && do_action) {
				cond_target.find("[data-factor]").data("factor", tmp_custom_value);
			}
			// select option
			else if (action == "select_option" && do_action) {
				if (cond_target_type == "radio") {
					_this.radio_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), true);
				}
				else if (cond_target_type == "checkbox") {
					_this.checkbox_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), true);
				}
				else if (cond_target_type == "dropdown") {
					cond_target.find(":selected").removeAttr("selected");
					cond_target.find("option[data-optionid='" + tmp_custom_value + "']").prop("selected", "selected");
				}
			} 
			// deselect option
			else if (action == "deselect_option" && do_action) {
				if (cond_target_type == "radio") {
					_this.radio_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
				}
				else if (cond_target_type == "checkbox") {
					_this.checkbox_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
				}
				else if (cond_target_type == "dropdown") {
					cond_target.find(":selected").removeAttr("selected");
				}
			}
			// show option
			else if (action == "show_option") {
				if (do_action) {
					if (cond_target_type == "radio" || cond_target_type == "checkbox") {
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").show();
					}
					else if (cond_target_type == "dropdown") {
						cond_target.find("option[data-optionid='" + tmp_custom_value + "']").removeAttr("hidden");
					}
				}
				// hide option
				else {
					if (cond_target_type == "radio") {
						_this.radio_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").hide();
					}
					else if (cond_target_type == "checkbox") {
						_this.checkbox_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").hide();
					}
					else if (cond_target_type == "dropdown") {
						cond_target.find(":selected").removeAttr("selected");
						cond_target.find("option[data-optionid='" + tmp_custom_value + "']").attr("hidden", true);
					}
				}
			} 
			// hide option
			else if (action == "hide_option") {
				if (do_action) {
					if (cond_target_type == "radio") {
						_this.radio_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").hide();
					}
					else if (cond_target_type == "checkbox") {
						_this.checkbox_change_state(cond_target.find("input[data-optionid='" + tmp_custom_value + "']"), false);
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").hide();
					}
					else if (cond_target_type == "dropdown") {
						cond_target.find("option[data-optionid='" + tmp_custom_value + "']").attr("hidden", true).removeAttr("selected");
					}
				}
				// show option
				else {
					if (cond_target_type == "radio" || cond_target_type == "checkbox") {
						cond_target.find("input[data-optionid='" + tmp_custom_value + "']").closest(".ezfc-element-single-option-container").show();
					}
					else if (cond_target_type == "dropdown") {
						cond_target.find("option[data-optionid='" + tmp_custom_value + "']").removeAttr("hidden");
					}
				}
			}
			// activate element
			else if (action == "activate") {
				// activate
				if (do_action) {
					// submit button
					if (cond_target_type == "submit") {
						cond_target.prop("disabled", false);
					}
					// group
					else if (cond_target_type == "group") {
						var tmp_cond_targets = cond_target.find("[data-calculate_enabled]");
						tmp_cond_targets.data("calculate_enabled", 1);
						tmp_cond_targets.attr("data-calculate_activated", 1);
					}
					// element
					else {
						cond_target.data("calculate_enabled", 1);
						cond_target.attr("data-calculate_activated", 1);
					}
				}
				// deactivate
				else if (cond_notoggle_element != 1) {
					// submit button
					if (cond_target_type == "submit") {
						cond_target.prop("disabled", true);
					}
					// group
					else if (cond_target_type == "group") {
						var tmp_cond_targets = cond_target.find("[data-calculate_enabled]");
						tmp_cond_targets.data("calculate_enabled", 0);
						tmp_cond_targets.attr("data-calculate_activated", 0);
					}
					// element
					else {
						cond_target.data("calculate_enabled", 0);
						cond_target.attr("data-calculate_activated", 0);
					}
				}
			}
			// activate option
			else if (action == "activate_option") {
				var find_option_id = "[data-optionid='" + cond.option_index_value[ic] + "']";

				// activate
				if (do_action) {
					cond_target.find(find_option_id).prop("disabled", false).removeClass("force-disabled");
				}
				// deactivate
				else {
					cond_target.find(find_option_id).prop("disabled", true).addClass("force-disabled");
				}
			}
			// deactivate element
			else if (action == "deactivate") {
				// deactivate
				if (do_action) {
					// submit button
					if (cond_target_type == "submit") {
						cond_target.prop("disabled", true);
					}
					// group
					else if (cond_target_type == "group") {
						var tmp_cond_targets = cond_target.find("[data-calculate_enabled]");
						tmp_cond_targets.data("calculate_enabled", 0);
						tmp_cond_targets.attr("data-calculate_activated", 0);
					}
					// element
					else cond_target.data("calculate_enabled", 0);
					cond_target.attr("data-calculate_activated", 0);
				}
				// activate
				else if (cond_notoggle_element != 1) {
					// submit button
					if (cond_target_type == "submit") {
						cond_target.prop("disabled", false);
					}
					// group
					else if (cond_target_type == "group") {
						var tmp_cond_targets = cond_target.find("[data-calculate_enabled]");
						tmp_cond_targets.data("calculate_enabled", 1);
						tmp_cond_targets.attr("data-calculate_activated", 1);
					}
					// element
					else {
						cond_target.data("calculate_enabled", 1);
						cond_target.attr("data-calculate_activated", 1);
					}
				}
			}
			// deactivate option
			else if (action == "deactivate_option") {
				var find_option_index = "[data-optionid='" + cond.option_index_value[ic] + "']";

				// activate
				if (do_action) {
					cond_target.find(find_option_index).prop("disabled", true).addClass("force-disabled");
				}
				// deactivate
				else {
					cond_target.find(find_option_index).prop("disabled", false).removeClass("force-disabled");
				}
			}
			// load form
			else if (action == "redirect" && do_action) {
				// set message
				var message_wrapper = $(form).parents(".ezfc-wrapper").find(".ezfc-message");
				message_wrapper.text(_this.form_vars[form_id].redirect_text).fadeIn();

				// hide the form
				$(form).fadeOut();

				setTimeout(function() {
					window.location.href = cond.redirects[ic];
				}, _this.form_vars[form_id].redirect_timer * 1000);
			}
			// steps
			else if ((action == "step_goto" || action == "step_prev" || action == "step_next") && do_action) {
				var current_step = parseInt($(form).find(".ezfc-step-active").data("step"));
				var next_step = 0;

				switch (action) {
					case "step_prev":
						if (current_step == 0) return;
						next_step = current_step - 1;
					break;

					case "step_next":
						var step_length = $(form).find(".ezfc-step-start").length;
						if (current_step == step_length - 1) return;

						next_step = current_step + 1;
					break;

					case "step_goto":
						var step_goto = $(form).find(".ezfc-step-start[data-id='" + cond.target[ic] + "']");
						if (step_goto.length < 1) return;

						next_step = parseInt(step_goto.data("step"));
					break;
				}

				_this.set_step($(form), next_step, 0);
			}
			// set min/max selectable
			else if (action == "set_min_selectable" && do_action) {
				cond_target.attr("data-min_selectable", tmp_custom_value);
				_this.checkbox_change(cond_target);
			}
			else if (action == "set_max_selectable" && do_action) {
				cond_target.attr("data-max_selectable", tmp_custom_value);
				_this.checkbox_change(cond_target);
			}
			// add / remove class
			else if (action == "add_class") {
				var tmp_action = do_action ? "addClass" : "removeClass";
				cond_target[tmp_action](tmp_custom_value);
			}
			else if (action == "remove_class") {
				var tmp_action = do_action ? "removeClass" : "addClass";
				cond_target[tmp_action](tmp_custom_value);	
			}
			// color
			else if (action == "set_color" && do_action) {
				cond_target.find("input, select, .ezfc-price, .ezfc-element-checkbox-text").css("color", tmp_custom_value);

				if (cond_target_type == "submit") cond_target.css("color", tmp_custom_value);
			}
			// show / hide elements
			else {
				if (action == "show") {
					js_action         = "removeClass";
					js_counter_action = "addClass";
				}
				else if (action == "hide") {
					js_action         = "addClass";
					js_counter_action = "removeClass";
				}
				else return;

				if (do_action) {
					cond_target[js_action]("ezfc-hidden ezfc-custom-hidden");
					
					// fade in
					if (action == "show") {
						cond_target.addClass("ezfc-fade-in");
					}
					// fade out
					else if (cond_target.is(":visible")) {
						cond_target.fadeOut(500, function() {
							cond_target.removeClass("ezfc-fade-in");

							if (_this.form_vars[form_id].clear_selected_values_hidden == 1) {
								// clear values
								_this.clear_hidden_values_element();
							}
						});
					}
				}
				// only do the counter action when notoggle is not enabled
				else if (cond_notoggle_element != 1) {
					cond_target[js_counter_action]("ezfc-hidden ezfc-custom-hidden");

					if (action == "show") { cond_target.removeClass("ezfc-fade-in"); }
					else { cond_target.addClass("ezfc-fade-in"); }
				}
			}
		});
	};

	/**
		calculate single element
	**/
	this.calculate_element = function(form_id, element_id, loop_price) {
		var $form      = $("#ezfc-form-" + form_id);
		var $element   = $("#ezfc_element-" + element_id);
		var element_js = ezfc_vars.element_vars[form_id][element_id];

		// check if form and element exist
		if (!$form || !$element) {
			console.log("Unable to find form #" + form_id + " or element #" + element_id);
			return;
		}

		var calc_rows       = typeof element_js.calculate === "undefined" ? [] : element_js.calculate;
		var calc_enabled    = $element.data("calculate_enabled");
		var add_to_price    = $element.data("add_to_price");
		var el_type         = $element.data("element");
		var form_has_steps  = _this.form_vars[form_id].has_steps;
		var overwrite_price = $element.data("overwrite_price");
		var price           = 0;

		if (el_type == "subtotal" || el_type == "custom_calculation" || el_type == "extension") {
			price = loop_price;
		}

		if (el_type == "custom_calculation" || el_type == "extension") {
			calc_rows = [];
		}
	
		// dropdowns / radios / checkboxes could contain more values
		var calc_list = $element.find(".ezfc-element-numbers, .ezfc-element-input-hidden, .ezfc-element-subtotal, .ezfc-element-daterange-container, .ezfc-element-set, .ezfc-element-extension, :selected, :checked, .ezfc-element-custom-calculation");

		// these operators do not need any target or value
		var operator_no_check = ["ceil", "floor", "round", "abs", "subtotal"];

		$(calc_list).each(function(cl, cl_object) {
			if (typeof calc_rows === "undefined") return;

			var el_settings = {};
			if ($(this).data("settings")) {
				el_settings = $(this).data("settings");
			}

			// skip when calculation is disabled for hidden elements
			var check_visible = _this.element_is_visible(form_id, element_id);

			if (!check_visible && (!el_settings.hasOwnProperty("calculate_when_hidden") || el_settings.calculate_when_hidden == 0) && el_type != "hidden" && el_type != "custom_calculation") {
				_this.add_debug_info("calculate", $element, "Skipped as element is hidden and calculate_when_hidden is not enabled.");
				return;
			}

			// no target or values to calculate with were found. skip for subtotals / hidden.
			if ((!calc_enabled || calc_enabled == 0) &&
				!calc_rows.targets &&
				!calc_rows.values &&
				el_type != "set" &&
				el_type != "subtotal" &&
				el_type != "hidden" &&
				el_type != "extension" &&
				el_type != "custom_calculation") {
				_this.add_debug_info("calculate", $element, "No target or values were found to calculate with. Subtotal, Hidden and Set elements are skipped.");
				return;
			}

			// check if calculation is enabled for _this element
			if ((!calc_enabled || calc_enabled == 0) && el_type != "custom_calculation") {
				_this.add_debug_info("calculate", $element, "Calculation is disabled.");
				return;
			}

			var factor       = parseFloat($(cl_object).data("factor"));
			var value_raw    = $(cl_object).val();
			var value        = _this.get_value_from_element($element, null, false);
			var value_pct    = value / 100;
			var value_is_pct = value_raw.indexOf("%") >= 0;

			// default values
			if (!value || isNaN(value)) value = 0;
			if ((!factor || isNaN(factor)) && factor !== 0) factor = 1;

			// set addprice to value at first
			var addPrice = value;

			// basic calculations
			switch (el_type) {
				case "numbers":
				case "extension":
				case "hidden":
					addPrice = value;
				break;

				case "dropdown":
				case "radio":
				case "checkbox":
					addPrice = parseFloat($(cl_object).data("value"));
					if (isNaN(addPrice)) addPrice = 0;
				break;

				case "subtotal":
					addPrice = loop_price;
				break;

				case "daterange":
					var tmp_target_value = [
						// from
						$(cl_object).find(".ezfc-element-daterange-from").datepicker("getDate"),
						// to
						$(cl_object).find(".ezfc-element-daterange-to").datepicker("getDate")
					];

					addPrice = _this.jqueryui_date_diff(tmp_target_value[0], tmp_target_value[1], $(cl_object).data("workdays_only")) * factor;
				break;

				// custom calculation function
				case "custom_calculation":
					var function_name = $($element).find(".ezfc-element-custom-calculation").data("function");

					try {
						addPrice = window[function_name](price);
					}
					catch (err) {
						_this.add_debug_info("custom_calculation", $element, "--- Custom Calculation Error ---");
						addPrice = 0;
					}

					if (calc_enabled) {
						addPrice = parseFloat(addPrice);
					}

					$($element).find(".ezfc-element-custom-calculation-input").val(addPrice);

					// improve performance here
					if (ezfc_vars.debug_mode == 2) {
						var function_text = $($element).find(".ezfc-element-custom-calculation script").text();
						_this.add_debug_info("custom_calculation", $element, "custom_calculation:\n" + function_text);
					}
				break;
			}

			// percent calculation
			if (value_is_pct) {
				addPrice = price * value_pct * 100;
			}

			// check if any advanced calculations are present
			if (typeof calc_rows[0] !== "undefined") {
				// transfer "open" bracket data to "close" bracket
				for (var c_open in calc_rows) {
					if (calc_rows[c_open].target == "__open__" && typeof calc_rows[c_open].reference_index === "undefined") {
						// check for valid prio
						calc_rows[c_open].prio = parseInt(calc_rows[c_open].prio);
						if (isNaN(calc_rows[c_open].prio)) {
							calc_rows[c_open].prio = 0;
						}

						for (var c_close in calc_rows) {
							if (c_open == c_close) continue;

							calc_rows[c_close].prio = parseInt(calc_rows[c_close].prio);
							if (isNaN(calc_rows[c_close].prio)) {
								calc_rows[c_close].prio = 0;
							}

							// find next close bracket with the same priority as the open bracket
							if (calc_rows[c_close].target == "__close__" && calc_rows[c_open].prio == calc_rows[c_close].prio && typeof calc_rows[c_open].reference_index === "undefined" && typeof calc_rows[c_close].reference_index === "undefined") {
								calc_rows[c_close].operator        = calc_rows[c_open].operator;
								calc_rows[c_close].reference_index = c_open;
								calc_rows[c_open].reference_index  = c_close;
							}
						}
					}
				}

				// iterate through all operators elements
				$.each(calc_rows, function(n, calc_row) {
					// no calculation operator
					if (!calc_row.operator && calc_row.target != "__close__") {
						_this.add_debug_info("calculate", $element, "#" + n + ": No operator found here.");
						return;
					}

					// skip open bracket
					if (calc_row.target == "__open__") {
						calc_row.value = addPrice;
						addPrice = 0;
						return;
					}

					var calc_target = [];
					// operator needs a target
					if ($.inArray(calc_row.operator, operator_no_check) === -1 && calc_row.target != "__open__" && calc_row.target != "__close__") {
						// target to be calculated with
						calc_target = $("#ezfc_element-" + calc_row.target);
						el_settings_target = calc_target.find("input").data("settings");

						// skip hidden element only if calculate_when_hidden is false
						if (calc_target.hasClass("ezfc-custom-hidden") && el_settings_target && (el_settings_target.hasOwnProperty("calculate_when_hidden") && el_settings_target.calculate_when_hidden == 0)) {
							_this.add_debug_info("calculate", $element, "#" + n + ": Skipping this element as it is conditionally hidden.");

							// set price to 0 or the previous subtotal price will be used (falsely)
							if (n == 0 && calc_row.operator == "equals") addPrice = 0;
							return;
						}
					}

					// custom value used when no target was found
					var calc_value = calc_row.value;

					// use value from target
					var target_value;
					var calc_target_id = 0;

					// target value is value of next close bracket
					if (calc_row.target == "__close__") {
						if (typeof calc_rows[calc_row.reference_index] === "undefined") return;

						target_value = addPrice;
						addPrice = calc_rows[calc_row.reference_index].value;
					}
					else {
						if (calc_target.length > 0) {
							calc_target_id = calc_target.data("id");

							if (typeof calc_row.use_calculated_target_value === "undefined") {
								calc_row.use_calculated_target_value = 0;
							}

							// raw value
							if (calc_row.use_calculated_target_value == 0) {
								target_value = _this.get_value_from_element(calc_target, null, false);
							}
							// calculated target value with subtotal
							else if (calc_row.use_calculated_target_value == 1) {
								target_value = _this.get_target_subtotal_value(form_id, calc_target_id) + _this.get_calculated_element_value(form_id, calc_target_id);
							}
							// calculated target value without subtotal
							else if (calc_row.use_calculated_target_value == 2) {
								target_value = _this.get_calculated_element_value(form_id, calc_target_id);
							}
							// raw value without factor
							else if (calc_row.use_calculated_target_value == 3) {
								target_value = _this.get_value_from_element(calc_target, null, false, true);
							}
						}
						else if (calc_value != 0) {
							target_value = parseFloat(calc_value);
						}
					}

					if (!target_value || isNaN(target_value)) target_value = 0;
					// use element precision
					if (el_settings.hasOwnProperty("precision") && el_settings.precision != "") {
						target_value = parseFloat(_this.roundTo(target_value, el_settings.precision));
					}

					switch (calc_row.operator) {
						case "add":	addPrice += target_value;
						break;

						case "subtract": addPrice -= target_value;
						break;

						case "multiply": addPrice *= target_value;
						break;

						case "divide": 
							if (target_value == 0) {
								_this.add_debug_info("calculate", $element, "#" + n + ": Division by 0.");
								addPrice = 0;
							}
							else {
								addPrice /= target_value;

								// still necessary?
								if ($(cl_object).data("calculate_before") == "1") {
									overwrite_price = 1;
									addPrice = target_value / value;
								}
							}
						break;

						case "equals": addPrice = target_value;
						break;

						case "power": addPrice = Math.pow(addPrice, target_value);
						break;

						case "ceil": addPrice = Math.ceil(addPrice);
						break;

						case "floor": addPrice = Math.floor(addPrice);
						break;

						case "round": addPrice = Math.round(addPrice);
						break;

						case "abs": addPrice = Math.abs(addPrice);
						break;

						case "subtotal": addPrice = loop_price;
						break;

						case "log":
							if (target_value == 0) return;
							addPrice = Math.log(target_value);
						break;
						case "log2":
							if (target_value == 0) return;
							addPrice = Math.log2(target_value);
						break;
						case "log10":
							if (target_value == 0) return;
							addPrice = Math.log10(target_value);
						break;

						case "sqrt": addPrice = Math.sqrt(addPrice);
						break;
					}

					_this.add_debug_info("calculate", $element, "#" + n + ": operator = " + calc_row.operator + "\ntarget_value = " + target_value + "\ntarget_element = #" + calc_target_id + "\ncalc_value = " + calc_value + "\naddPrice = " + addPrice);
				});
			}

			// add calculated price to total price
			if (add_to_price == 1) {
				price += addPrice;
			}
			else if (add_to_price == 2) {
				price = addPrice;
			}

			// overwrite price
			if (overwrite_price == 1) {
				price = addPrice;
			}

			_this.add_debug_info("calculate", $element, "===\nprice = " + price + "\naddPrice = " + addPrice + "\nloop_price = "+ loop_price + "\nvalue = " + value + "\nfactor = " + factor);
		});

		return price;
	};

	/**
		element calculations
	**/
	this.calculate_element_loop = function(form_id, el_object, el_type, price) {
		if (!form_id) form_id = el_object.closest(".ezfc-form").data("id");

		var calc_enabled    = el_object.data("calculate_enabled");
		var overwrite_price = el_object.data("overwrite_price");
		var add_to_price    = el_object.data("add_to_price");
		var is_number       = el_object.data("is_number");
		var is_currency     = el_object.data("is_currency");

		var tmp_price;
		var addPrice = this.calculate_element(form_id, el_object.data("id"), price);

		// add calculated price to total price
		if (calc_enabled == 1) {
			if (add_to_price >= 1) {
				if (overwrite_price == 1) {
					price = addPrice;
				}
				else {
					price += addPrice;
				}
			}
			// for subtotal / set elements only (doesn't interfere with calculation but use the calculated price as text)
			else {
				if (overwrite_price == 1) {
					tmp_price = addPrice;
				}
			}
		}

		if (el_type == "subtotal" || el_type == "set" || el_type == "custom_calculation") {
			if (add_to_price == 1) {
				tmp_price = overwrite_price==1 ? price : addPrice;
			}
			else if (add_to_price == 2) {
				tmp_price = addPrice;
			}
			else {
				tmp_price = addPrice;
			}

			var precision = 2;
			var element_settings = el_object.find("input").data("settings");
			if (element_settings) {
				precision = element_settings.precision;
			}

			var price_to_write = tmp_price;

			if (is_number == 1) {
				price_to_write = this.normalize_value(tmp_price, el_object, precision);

				// format currency
				if (is_currency == 1) {
					price_to_write = this.format_price(form_id, price_to_write, null, null, true);
				}
			}

			el_object.find("input").val(price_to_write);
		}

		return price;
	};

	/**
		discount calculations
	**/
	this.calculate_discounts = function(form, form_id, el_object, el_type, price) {
		var el_id = el_object.data("id");
		var overwrite_price = el_object.data("overwrite_price") == 1;

		// check if discounts exist
		if (!el_id || typeof ezfc_vars.element_vars[form_id][el_id].discount === "undefined") return price;
		var discount = ezfc_vars.element_vars[form_id][el_id].discount;

		// check if there should be discount actions
		if (discount) {
			var discount_range_min_values = discount.range_min;
			var discount_range_max_values = discount.range_max;
			var discount_operator_values  = discount.operator;
			var discount_value_values     = discount.values;

			var el_value = 0;
			var factor   = 1;

			// get selected value from input fields
			if (el_type == "input" || el_type == "numbers" || el_type == "subtotal" || el_type == "hidden" || el_type == "extension") {
				var el_input = el_object.find("input");

				factor = parseFloat(el_input.data("factor"));
				if ((!factor || isNaN(factor)) && factor !== 0) factor = 1;

				el_value = this.normalize_value(el_input.val(), el_object);
			}
			// get selected value from dropdowns
			else if (el_type == "dropdown") {
				el_value = parseFloat(el_object.find(":selected").data("value"));
			}
			// get selected value from radio
			else if (el_type == "radio") {
				el_value = parseFloat(el_object.find(":checked").data("value"));
			}
			// get selected values from checkboxes
			else if (el_type == "checkbox") {
				el_value = 0;
				el_object.find(":checked").each(function(ct, ct_el) {
					el_value += parseFloat($(ct_el).data("value"));
				});
			}
			// get amount of days from date range
			else if (el_type == "daterange") {
				var tmp_target_value = [
					// from
					el_object.find(".ezfc-element-daterange-from").datepicker("getDate"),
					// to
					el_object.find(".ezfc-element-daterange-to").datepicker("getDate")
				];

				el_value = _this.jqueryui_date_diff(tmp_target_value[0], tmp_target_value[1], el_object.data("workdays_only"));
			}

			// go through all discounts
			$.each(discount_operator_values, function(id, operator) {
				if (discount_value_values[id].length < 1) return;
				
				if (!discount_range_min_values[id] && discount_range_min_values[id] !== 0) discount_range_min_values[id] = Number.NEGATIVE_INFINITY;
				if (!discount_range_max_values[id] && discount_range_max_values[id] !== 0) discount_range_max_values[id] = Number.POSITIVE_INFINITY;

				var discount_value_write_to_input;

				if (el_value >= parseFloat(discount_range_min_values[id]) && el_value <= parseFloat(discount_range_max_values[id])) {
					var disc_value = parseFloat(discount_value_values[id]);
					var discount_value_operator;

					switch (operator) {
						case "add":
							discount_value_operator = disc_value;
							discount_value_write_to_input = price + discount_value_operator;

							if (overwrite_price) {
								price = discount_value_write_to_input;
							}
							else {
								price += discount_value_write_to_input;
							}
						break;

						case "subtract":
							discount_value_operator = disc_value;
							discount_value_write_to_input = price - discount_value_operator;

							if (overwrite_price) {
								price = discount_value_write_to_input;
							}
							else {
								price -= discount_value_write_to_input;
							}
						break;

						case "percent_add":
							discount_value_operator = el_value * factor * (disc_value / 100);
							discount_value_write_to_input = price + discount_value_operator;

							price = discount_value_write_to_input;
						break;

						case "percent_sub":
							discount_value_operator = el_value * factor * (disc_value / 100);
							discount_value_write_to_input = price - discount_value_operator;

							price = discount_value_write_to_input;
						break;

						case "equals":
							discount_value_operator = disc_value;
							discount_value_write_to_input = discount_value_operator;

							price = discount_value_write_to_input;
						break;

						case "factor":
							discount_value_operator = el_value * disc_value;
							discount_value_write_to_input = discount_value_operator;

							var $factor_element = el_object.find("[data-factor]");
							var tmp_factor = $factor_element.data("factor");
							// recalc when factor changed
							if (tmp_factor != disc_value) {
								$factor_element.data("factor", disc_value);
								// not sure why but it needs to trigger two times before the calculation takes place at all
								$factor_element.trigger("change");
								$factor_element.trigger("change");
							}
						break;
					}

					if (el_type == "subtotal" && !isNaN(discount_value_write_to_input)) {
						discount_value_write_to_input = _this.normalize_value(discount_value_write_to_input, el_object);
						$(el_object).find("input").val(discount_value_write_to_input);
					}

					_this.add_debug_info("discount", el_object, "discount = " + discount_value_operator + "\nprice after discount = " + price);
				}
			});
		}


		return price;
	};

	/**
		set values for set elements
	**/
	this.calculate_set_elements = function(form, form_id, el_object, el_type, price) {
		var set_operator     = el_object.data("set_operator");
		var tmp_targets      = el_object.data("set_elements");
		var allow_zero       = el_object.data("set_allow_zero") == 1;
		var set_dom_selector = el_object.data("set_dom_selector");
		var targets          = [];
		var value_to_write;

		// check if there should be conditional actions
		if (!tmp_targets && !set_dom_selector) return;

		if (set_dom_selector) {
			targets = $(set_dom_selector);
		}
		else {
			var tmp_targets_elements = tmp_targets.toString().split(",");

			$.each(tmp_targets_elements, function(i, v) {
				var $target_object = $("#ezfc_element-" + v);
				if (!$target_object) return;

				targets.push($target_object);
			});
		}

		$.each(targets, function(i, tmp_element) {
			var $element = $(tmp_element);
			var el_value = _this.get_value_from_element($element, null, false);

			// check for 0
			if (!allow_zero && el_value == 0) return;

			// first element
			if (i == 0) {
				value_to_write = el_value;
				return;
			}

			switch (set_operator) {
				case "min":
					if (el_value < value_to_write) value_to_write = el_value;
				break;

				case "max":
					if (el_value > value_to_write) value_to_write = el_value;
				break;

				case "avg":
				case "sum":
					value_to_write += el_value;
				break;

				case "dif":
					value_to_write -= el_value;
				break;

				case "prod":
					value_to_write *= el_value;
				break;

				case "quot":
					if (el_value != 0) value_to_write /= el_value;
				break;
			}
		});

		if (set_operator == "avg") {
			value_to_write = value_to_write / targets.length;
		}

		value_to_write = this.normalize_value(value_to_write, el_object);

		el_object.find("input").val(value_to_write);
	};

	// price calculation
	this.calculate_price = function(form) {
		var form_id = $(form).data("id");
		var price = 0;

		// reset subtotals
		this.subtotals[form_id] = [];

		// find all elements first
		$.each(_this.$form_elements[form_id], function(i) {
			var $element    = _this.$form_elements[form_id][i];
			var el_id       = $element.data("id");
			var el_type     = $element.data("element");

			var el_settings = {};
			if ($element.find("input").data("settings")) {
				el_settings = $element.find("input").data("settings");
			}

			// get external value if present
			_this.calculate_get_external_values(form, form_id, $element, el_type);

			// check conditionals
			_this.calculate_conditionals(form, form_id, $element, el_type);

			var calculate_when_hidden = 0;
			if (typeof el_settings.calculate_when_hidden !== "undefined") {
				calculate_when_hidden = parseInt(el_settings.calculate_when_hidden);
			}

			// check if element is hidden
			if (!_this.element_is_visible(form_id, el_id) && !calculate_when_hidden && el_type != "hidden") return;

			// set elements
			_this.calculate_set_elements(form, form_id, $element, el_type);

			// process calculations
			var element_loop_price = _this.calculate_element_loop(form_id, $element, el_type, price);
			price = element_loop_price;

			// discount
			price = _this.calculate_discounts(form, form_id, $element, el_type, price);

			// TODO: re-calculate elements after discounts (factor)

			if ($element.data("calculate_enabled")) {
				_this.subtotals[form_id].push({
					el_id: el_id,
					price: price
				});
			}

			// check conditionals again
			_this.calculate_conditionals(form, form_id, $element, el_type);
		});

		return price;
	};

	this.set_price = function(form, price_old, force_price) {
		var form_id = $(form).data("id");

		// calculate price
		if (force_price) {
			price = force_price;
		}
		else if (!price_old || price_old !== 0) {
			price = this.calculate_price($(form));
		}

		this.set_subtotal_values($(form));

		// show price after request
		if (this.form_vars[form_id].price_show_request == 1 && this.form_vars[form_id].price_requested == 0) {
			this.price_request_toggle(form_id, false);

			return;
		}

		if (typeof this.price_old_global[form_id] === "undefined") this.price_old_global[form_id] = 0;
		if (this.price_old_global[form_id] == price) return;

		if (this.form_vars[form_id].counter_duration != 0) {
			$(form).find(".ezfc-price-value").countTo({
				from: _this.price_old_global[form_id],
				to: price,
				speed: this.form_vars[form_id].counter_duration,
				refreshInterval: this.form_vars[form_id].counter_interval,
				formatter: function (value, options) {
					return _this.format_price(form_id, value);
				}
			});
		}
		else {
			$(form).find(".ezfc-price-value").text(this.format_price(form_id, price));
		}

		this.price_old_global[form_id] = price;
	};

	this.format_price = function(form_id, price, currency, custom_price_format, format_with_currency) {
		var form_price_format = this.defaultFormat;
		var form_currency     = currency || this.form_vars[form_id].currency;

		// use price format from form settings
		if (this.form_vars[form_id].price_format && this.form_vars[form_id].price_format.length > 0) {
			form_price_format = this.form_vars[form_id].price_format;
		}

		// if defined, use custom price format
		if (custom_price_format && custom_price_format.length > 0) {
			form_price_format = custom_price_format;
		}

		if (isNaN(price)) price = 0;

		var price_formatted = 0;
		if (isFinite(price)) {
			price_formatted = numeral(price).format(form_price_format);

			// replace trailing zeros: 10,00 -> 10,-
			if (ezfc_vars.price_format_replace_trailing_zeros.enabled == 1) {
				// price sometimes looks like 8.0000000000017, so take the first decimal numbers and check if it's an integer
				if (parseFloat(price).toFixed(6) % 1 === 0) {
					price_formatted += ezfc_vars.price_format_dec_point + ezfc_vars.price_format_replace_trailing_zeros.text;
				}
			}
		}

		// add currency symbol
		if (format_with_currency) {
			if (this.form_vars[form_id].currency_position == 0) {
				price_formatted = form_currency + price_formatted;
			}
			else {
				price_formatted = price_formatted + form_currency;
			}
		}

		return price_formatted;
	};

	// price request toggler
	this.price_request_toggle = function(form_id, enable, price) {
		var form = $(".ezfc-form[data-id='" + form_id + "']");

		// enable submit
		if (enable) {
			this.price_old_global[form_id] = 0;
			this.form_vars[form_id].price_requested = 1;
			// calculate form price so element values show the correct price
			this.calculate_price(form);
			// set request price
			this.set_price(form, null, price);
			// set submit button text
			this.set_submit_text(form);
		}
		else {
			this.form_vars[form_id].price_requested = 0;

			$(form).find(".ezfc-price-value").text(this.form_vars[form_id].price_show_request_before);
			$(form).find(".ezfc-submit").val(this.form_vars[form_id].submit_text.request);
		}
	};

	this.set_subtotal_values = function(form) {
		var element_list = ["subtotal", "set", "numbers", "custom_calculation"];

		$(element_list).each(function(el_i, el_e) {
			$(form).find("[data-element='" + el_e + "']").each(function(i, el) {
				var $tmp_element;

				// use different element for custom_js elements
				if (el_e == "custom_calculation") {
					$tmp_element = $(el).find(".ezfc-element-custom-calculation-input");
				}
				else {
					$tmp_element = $(el).find(".ezfc-element-" + el_e);
				}

				var value         = _this.normalize_value($tmp_element.val(), $tmp_element);
				var price_format  = null;

				var el_settings, text;

				if ($tmp_element.data("settings")) {
					el_settings  = $tmp_element.data("settings");
					price_format = el_settings.price_format;
				}

				text = _this.format_price($(form).data("id"), value, null, price_format);

				$(el).find(".ezfc-text").text(text);
			});
		});
	};

	this.scroll = function() {
		$(".ezfc-fixed-price").each(function() {
			var form_id = $(this).data("id");
			// form not yet initialised
			if (!_this.form_vars[form_id]) return;

			var active_scroll_class = "ezfc-fixed-price-scrolling";
			var offset              = $(this).offset();
			var form                = $(".ezfc-form[data-id='" + form_id + "']");
			var form_height         = form.outerHeight();
			var form_offset         = form.offset();
			var window_top          = $(window).scrollTop();
			var price_position_top  = parseFloat(_this.form_vars[form_id].price_position_scroll_top);

			var diff = form_offset.top - window_top - price_position_top;
			if (diff < 0 && diff > -form_height) {
				$(this).offset({ top: window_top + price_position_top });
				$(this).addClass(active_scroll_class);
			}
			else if (diff > 0 && offset.top > form_offset.top) {
				$(this).offset({ top: form_offset.top });
				$(this).removeClass(active_scroll_class);
			}
		});
	};

	// reset disabled fields and restore initial values (since they may have changed due to conditional logic). also, set the relevant submit button text
	this.reset_disabled_fields = function(form, error) {
		var $form_wrapper = $(form).closest(".ezfc-wrapper");

		$(form).find(".ezfc-custom-hidden").each(function() {
			$.each($(this).find("input, :selected"), function(i, v) {
				$(this).val($(this).data("index")).removeAttr("disabled");
			});
		});

		// also reset payment fields
		$form_wrapper.find(".ezfc-payment-submit, .ezfc-payment-cancel").prop("disabled", false);

		this.set_submit_text(form, error);
	};

	// reset the whole form
	this.reset_form = function($form) {
		this.init_form($form);

		// reset values
		$form.find(".ezfc-custom-element").each(function() {
			var el_type = $(this).data("element");
			var initvalue = $(this).find("[data-initvalue]").data("initvalue");

			switch (el_type) {
				case "checkbox":
					$(this).find("input").each(function() {
						initvalue = $(this).data("initvalue");

						if (initvalue == 1)
							$(this).prop("checked", true);
						else
							$(this).removeAttr("checked");
					});
				break;

				case "dropdown":
					$(this).find("option").removeAttr("selected");
					$(this).find("option[data-index='" + initvalue + "']").prop("selected", true);
				break;

				case "numbers":
					$(this).find("input").val(initvalue);
					var $sliders = $(this).find(".ezfc-slider-element");
					if ($sliders.length) $sliders.slider({ value: initvalue });
				break;

				case "radio":
					$(this).find("input").removeAttr("checked");
					$(this).find("input[data-initvalue]").prop("checked", true);
				break;

				case "textfield":
					$(this).find("textarea").val(initvalue);
				break;

				default:
					$(this).find("input").val(initvalue);
				break;
			}
		});

		$form.find(".ezfc-selected").removeClass("ezfc-selected");

		this.set_step($form, 0, 0);
		this.form_change($form);
	};

	this.set_step = function(form, new_step, verify) {
		var current_step = parseInt(form.find(".ezfc-step-active").data("step"));
		var step_wrapper = form.find(".ezfc-step[data-step='" + current_step + "']");
		var form_id      = form.data("id");

		if (current_step == new_step) return;

		// check ajax
		if (verify == 1 && this.form_vars[form_id].verify_steps == 1) {
			var submit_icon = form.find(".ezfc-step-submit-icon");
			submit_icon.fadeIn();

			this.form_submit(form, new_step - 1);

			$(".ezfc-has-hidden-placeholder").val("").removeClass("ezfc-has-hidden-placeholder");
			return;
		}

		// step indicator
		var step_indicator_start = parseInt(this.form_vars[form_id].step_indicator_start) - 1;

		step_wrapper.fadeOut(200, function() {
			var step_wrapper_next = form.find(".ezfc-step[data-step='" + new_step + "']");
			
			step_wrapper_next.fadeIn(200).addClass("ezfc-step-active");
			$(this).removeClass("ezfc-step-active");

			// maybe show step indicator
			if (new_step >= step_indicator_start) {
				form.find(".ezfc-step-indicator").fadeIn();
			}
			else {
				form.find(".ezfc-step-indicator").hide();
			}

			_this.scroll_to(step_wrapper_next);
		});

		form.find(".ezfc-step-indicator-item").each(function() {
			var step_dom = parseInt($(this).data("step"));
			$(this).removeClass("ezfc-step-indicator-item-active");
			
			if (step_dom <= new_step) {
				$(this).addClass("ezfc-step-indicator-item-active");
			}
		});

		return false;
	};

	this.scroll_to = function(element, custom_offset) {
		var element_offset = $(element).offset();

		if (typeof element_offset === "undefined" || ezfc_vars.auto_scroll_steps == 0) return;

		var offset_add = parseFloat(custom_offset) || parseFloat(ezfc_vars.scroll_steps_offset) || 50;
		var offset_scroll = element_offset.top + offset_add;

		$("html, body").animate({ scrollTop: offset_scroll });
	};

	this.get_value_from_element = function($el_object, e_id, is_text, ignore_factor, options) {
		var default_options = {
			return_array: false,
			return_value: "value"
		};
		options = $.extend({}, default_options, options);

		if (!$el_object) $el_object = $("#ezfc_element-" + e_id);
		if (!$el_object.length) {
			this.debug_message("Unable to find element #" + e_id);
			return 0;
		}

		var $input_element = $el_object.find("input");
		var el_type        = $el_object.data("element");
		var factor         = 1;
		var value_raw      = $el_object.find("input").val();
		var value_is_pct   = value_raw ? value_raw.indexOf("%") >= 0 : 0;
		var value          = this.normalize_value(value_raw, $el_object);
		var value_pct      = value / 100;

		// default values
		if (!value || isNaN(value)) value = 0;

		// set addprice to value first
		var return_value = is_text ? value : parseFloat(value);

		// return selected elements (checkboxes)
		if (options.return_value == "count") return_value = 0;

		// basic calculations
		switch (el_type) {
			case "input":
				return_value = value_raw;
			break;

			case "subtotal":
			case "numbers":
			case "hidden":
			case "extension":
			case "set":
				factor = parseFloat($input_element.data("factor"));

				if ((!factor || isNaN(factor)) && factor !== 0) factor = 1;

				if (options.return_value == "base") {
					var el_settings = $input_element.data("settings");
					value = value_raw = el_settings.value_base;
				}

				if (is_text) {
					return_value = value_raw;
				}
				else {
					return_value = ignore_factor ? value : value * factor;
				}
			break;

			case "dropdown":
			case "radio":
			case "checkbox":
				$el_object.find(":selected, :checked").each(function() {
					// return array of checked / selected values
					if (is_text) {
						// return array of selected values from index or actual value
						var read_value = options.return_value;

						if (typeof return_value !== "object" && read_value != "count") return_value = [];

						// check if name should be retrieved
						if (read_value == "name") {
							return_value.push($(this).text());
						}
						// option ID
						else if (read_value == "option_id") {
							return_value.push($(this).data("optionid"));
						}
						// count
						else if (read_value == "count") {
							return_value++;
						}
						// retrieve index or value
						else {
							return_value.push($(this).data(read_value));
						}
					}
					// add up values by default
					else {
						return_value += parseFloat($(this).data("value"));
					}
				});
			break;

			case "daterange":
				var tmp_target_value = [
					// from
					$el_object.find(".ezfc-element-daterange-from").datepicker("getDate"),
					// to
					$el_object.find(".ezfc-element-daterange-to").datepicker("getDate")
				];

				factor = parseFloat($el_object.find(".ezfc-element-daterange-from").data("factor"));

				if ((!factor || isNaN(factor)) && factor !== 0) factor = 1;

				// return dates as array
				if (is_text) {
					return_value = tmp_target_value;
				}
				// return date difference in days
				else {
					var days = _this.jqueryui_date_diff(tmp_target_value[0], tmp_target_value[1], $el_object.data("workdays_only"));

					return_value = ignore_factor ? days : days * factor;
				}
			break;

			// custom calculation function
			case "custom_calculation":
				return_value = $el_object.find(".ezfc-element-custom-calculation-input").val();

				if (!is_text) {
					return_value = parseFloat(return_value);
				}
			break;

			case "starrating":
				return_value = parseFloat($el_object.find(":checked").val());
				if (isNaN(return_value)) return_value = 0;
			break;
		}

		// percent calculation
		if (value_is_pct) {
			return_value = value_pct;
		}

		if (!is_text) {
			if (isNaN(return_value)) return_value = 0;
			
			return !return_value ? 0 : parseFloat(return_value);
		}

		return return_value;
	};

	/**
		checkbox was selected / deselected
	**/
	this.checkbox_change = function($element) {
		// check for min / max selectable
		var max_selectable = parseInt($element.attr("data-max_selectable"));

		if (!max_selectable) {
			$element.find("input:not(:checked):not(.force-disabled)").removeAttr("disabled");
		}

		if (max_selectable && max_selectable >= 0) {
			var selected = $element.find(":checked").length;

			if (selected >= max_selectable) {
				$element.find("input:not(:checked)").attr("disabled", "disabled");
			}
			else {
				$element.find("input:not(:checked):not(.force-disabled)").removeAttr("disabled");	
			}
		}
	};

	/**
		checkbox change state
	**/
	this.checkbox_change_state = function($checkbox, state, disable_change_trigger) {
		// exit when checkbox is disabled
		if ($checkbox.attr("disabled")) return false;

		// toggle if state wasn't defined
		if (state === undefined) {
			state = !$checkbox.is(":checked");
		}

		var has_image = $checkbox.closest(".ezfc-element-checkbox-container").hasClass("ezfc-element-option-has-image");
		// image checkboxes
		if (has_image) {
			var $image = $checkbox.siblings(".ezfc-element-option-image");

			// uncheck it -> remove selected class
			if (!state) {
				$checkbox.removeAttr("checked");
				$image.removeClass("ezfc-selected");
			}
			// check it
			else {
				$checkbox.attr("checked", "checked");
				$image.addClass("ezfc-selected");
			}
		}
		// simple checkbox
		else {
			$checkbox.prop("checked", state);
		}

		if (!disable_change_trigger) {
			$checkbox.trigger("change");
		}
	};

	/**
		radio change state
	**/
	this.radio_change_state = function($radio, state, disable_change_trigger) {
		// exit when checkbox is disabled
		if ($radio.attr("disabled")) return false;

		// toggle if state wasn't defined
		if (state === undefined) {
			state = !$radio.is(":checked");
		}

		var $parent_container = $radio.closest(".ezfc-element-radio-container");
		var $parent_wrapper   = $parent_container.closest(".ezfc-element-wrapper-radio");

		// remove selected class from images
		$parent_container.find(".ezfc-selected").removeClass("ezfc-selected");
		// "uncheck"
		$parent_container.find(".ezfc-element-radio-input").removeAttr("checked");

		var has_image = $parent_container.hasClass("ezfc-element-option-has-image");
		// image checkboxes
		if (has_image) {
			var $image = $radio.siblings(".ezfc-element-option-image");
			
			if (state) {
				// check radio input
				$radio.attr("checked", "checked");
				// remove selected class
				$parent_wrapper.find(".ezfc-selected").removeClass("ezfc-selected");
				// add selected class
				$image.addClass("ezfc-selected");
			}
		}
		// simple radio
		else if (state) {
			$radio.attr("checked", "checked");
		}

		if (!disable_change_trigger) {
			$radio.trigger("change");
		}
	};

	/**
		input with format listener changed
	**/
	this.input_format_listener_change = function($element) {
		var $el_wrapper = $element.parent().closest(".ezfc-element");
		var form_id     = $element.closest(".ezfc-form").data("id");
		var el_settings = {};
		var text_before = "";
		var text_after  = "";

		if ($element.data("settings")) {
			el_settings = $element.data("settings");
		}

		if (typeof el_settings.text_before !== "undefined") {
			text_before = el_settings.text_before;
		}
		if (typeof el_settings.text_after !== "undefined") {
			text_after = el_settings.text_after;
		}

		if ($el_wrapper.data("is_number") == 1) {
			// convert to numbers only (with decimal point)
			$element.on("focus click blur", function() {
				this.value = this.value.replace(/[^0-9\.,-]/g, "");
			});
		}
		else {
			$element.on("focus click", function() {
				var regex_text_before = _this.escape_regex(text_before);
				var regex_text_after  = _this.escape_regex(text_after);

				// replace prefix
				this.value = this.value.replace(new RegExp("^" + regex_text_before, ""), "");
				// replace suffix
				this.value = this.value.replace(new RegExp(regex_text_after + "$", ""), "");
			});
		}

		if ($el_wrapper.data("is_currency") == 1 && this.form_vars[form_id].format_currency_numbers_elements == 1) {
			// input blurred -> format price
			$element.on("blur", function() {
				this.value = _this.normalize_value(this.value, false, true, false);
				this.value = _this.format_price(form_id, this.value, null, null, true);
			});
		}

		$element.on("blur", function() {
			this.value = text_before + this.value + text_after;
		});
	};

	/**
		checks if element is visible
	**/
	this.element_is_visible = function(form_id, element_id) {
		var $element       = $("#ezfc_element-" + element_id);
		var form_has_steps = _this.form_vars[form_id].has_steps;

		// skip when calculation is disabled for hidden elements
		var check_visible = $element.is(":visible");

		// if we use steps, check_visible needs to be changed to ezfc-custom-hidden selector
		if (form_has_steps && !$element.closest(".ezfc-step-active").length) {
			check_visible = !$element.hasClass("ezfc-custom-hidden");
		}

		return check_visible;
	};

	this.clear_hidden_values = function(form) {
		var form_id = $(form).data("id");
		if (this.form_vars[form_id].clear_selected_values_hidden != 1) return;

		$(form).find(".ezfc-custom-hidden").each(function() {	
			$(this).find("input[type='text']").val("");
			$(this).find(":checkbox, :radio").prop("checked", false);
		});
	};

	this.clear_hidden_values_element = function(element) {
		var cond_target_type = element.data("element");

		if (cond_target_type == "input" || cond_target_type == "numbers" || cond_target_type == "subtotal") {
			cond_target.find("input").val("");
		}
		else if (cond_target_type == "dropdown") {
			cond_target.find(":selected").removeAttr("selected");
		}
		else if (cond_target_type == "radio" || cond_target_type == "checkbox") {
			cond_target.find(":checked").removeAttr("checked");
		}
	};

	this.escape_regex = function(s) {
		return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	};

	this.normalize_value = function(value, element, precision) {
		value = String(value);
		
		if (typeof element !== "undefined") {
			var $element = $(element);
			var settings;

			if ($element.data("settings")) settings = $element.data("settings");
			else if ($element.find("[data-settings]")) settings = $element.find("[data-settings]").data("settings");

			if (typeof settings !== "undefined" && typeof settings.text_before !== "undefined" && settings.text_after !== "undefined") {
				var regex_text_before = _this.escape_regex(settings.text_before);
				var regex_text_after  = _this.escape_regex(settings.text_after);

				// replace prefix
				value = value.replace(new RegExp("^" + regex_text_before, ""), "");
				// replace suffix
				value = value.replace(new RegExp(regex_text_after + "$", ""), "");
			}
		}

		var value_normalized = numeral(value).value();
		if (typeof precision !== "undefined") {
			value_normalized = value_normalized.toFixed(precision);
		}

		return value_normalized;
	};

	this.set_submit_text = function(form, error) {
		var form_id = $(form).data("id");

		// default submit text
		var $submit_button = $(form).find(".ezfc-submit");
		var submit_type = "default";
		var submit_text = this.form_vars[form_id].submit_text.default;
		
		// price request
		if (this.form_vars[form_id].price_show_request == 1) {
			if (!this.form_vars[form_id].price_requested || error) {
				submit_text = this.form_vars[form_id].submit_text.request;
				submit_type = "request";
			}

			if (error) {
				this.price_request_toggle(form_id, false);
				return false;
			}
		}
		// summary
		else if (this.form_vars[form_id].summary_enabled == 1 && this.form_vars[form_id].summary_shown == 0) {
			submit_type = "summary";
			submit_text = this.form_vars[form_id].submit_text.summary;
		}
		// paypal
		else if (this.form_vars[form_id].use_paypal == 1) {
			submit_type = "paypal";
			submit_text = this.form_vars[form_id].submit_text.paypal;
		}
		// stripe
		else if (this.form_vars[form_id].payment_force_stripe == 1) {
			submit_type = "stripe";
			submit_text = this.form_vars[form_id].submit_text.stripe;
		}
		// authorize
		/*else if (this.form_vars[form_id].payment_force_authorize == 1) {
			submit_type = "authorize";
			submit_text = this.form_vars[form_id].submit_text.authorize;
		}*/
		// woocommerce
		else if (this.form_vars[form_id].use_woocommerce == 1) {
			submit_type = "woocommerce";
			submit_text = this.form_vars[form_id].submit_text.woocommerce;
		}
		// default text
		else {
			submit_text = this.form_vars[form_id].submit_text.default;

			// check is payment method is used and use payment submit text
			var payment_method_element = $(form).find(".ezfc-element-wrapper-payment");
			if (payment_method_element.length > 0) {
				var payment_method_value = $(payment_method_element).find(":checked").data("value");

				if (typeof this.form_vars[form_id].submit_text[payment_method_value] !== "undefined") {
					submit_type = payment_method_value;
					submit_text = this.form_vars[form_id].submit_text[payment_method_value];
				}
			}
		}

		$submit_button.val(submit_text);
		$submit_button.data("type", submit_type);
	};

	/**
		prevent enter key to trigger the click-event on step-buttons since pressing enter would submit the form and move a step backwards in the last step
	**/
	this.prevent_enter_step_listener = function($elements, $form) {
		// step prevent enter keypress
		$($elements).keypress(function(e) {
			// normalize
			var key = e.keyCode || e.which;

			if (e.which == 13) {
				//_this.form_submit($form, -1);
				e.preventDefault();
			}
		});
	};

	/**
		js hooks for advanced customization purposes
	**/
	this.call_hook = function(hook_name, args) {
		var func = window[hook_name];

		if (typeof func !== "function") return;

		args = args || {};

		func(args);
	};

	/**
		stripe response handler
	**/
	this.stripe_response_handler = function(status, response) {
		var $form = $(".ezfc-form[data-id='" + _this.payment_form_id + "']");
		var $form_payment = $(".ezfc-payment-form[data-form_id='" + _this.payment_form_id + "']");

		if (response.error) {
			_this.debug_message(response.error.message);

			$("#ezfc-payment-message-" + _this.payment_form_id).text(response.error.message);
			$form_payment.find(".ezfc-payment-submit, .ezfc-payment-cancel").prop("disabled", false);

			setTimeout(function() {
				$("#ezfc-payment-message-" + _this.payment_form_id).text("");
			}, 7500);
		}
		else {
			// Get the token ID:
			var token = response.id;

			// Insert the token ID into the form so it gets submitted to the server:
			$form.find("#ezfc-stripetoken-" + _this.payment_form_id).val(token);

			// Submit the form:
			_this.form_submit($form, -1, "stripe-checkout");
		}
	};

	/**
		authorize create token
	**/
	this.authorize_create_token = function(id) {
		var secureData = {}; authData = {}; cardData = {};
		var $form_payment = $("#ezfc-authorize-form-" + id);

		cardData.cardNumber = $("#ezfc-element-payment-authorize-card-number-" + id).val();
		cardData.cardNumber = cardData.cardNumber.replace(/ /g, ""); // remove whitespaces
		cardData.month      = $("#ezfc-element-payment-authorize-expiry-month-" + id).val();
		cardData.year       = $("#ezfc-element-payment-authorize-expiry-year-" + id).val();
		cardData.cardCode   = $("#ezfc-element-payment-authorize-cvc-" + id).val();
		secureData.cardData = cardData;

		authData.clientKey  = ezfc_vars.authorize.client_key;
		authData.apiLoginID = ezfc_vars.authorize.api_login_id;
		secureData.authData = authData;

		Accept.dispatchData(secureData, _this.authorize_response_handler);
	};

	/**
		authorize response handler
	**/
	this.authorize_response_handler = function(response) {
		var $form = $(".ezfc-form[data-id='" + _this.payment_form_id + "']");
		var $form_payment = $(".ezfc-payment-form[data-form_id='" + _this.payment_form_id + "']");
		var form_id = $form.data("id");

		if (response.messages.resultCode === "Error") {
			$form_payment.find(".ezfc-payment-submit, .ezfc-payment-cancel").prop("disabled", false);

			var error_messages = [];
			for (var i = 0; i < response.messages.message.length; i++) {
				error_messages.push(response.messages.message[i].text);
				_this.debug_message(response.messages.message[i].code + ": " + response.messages.message[i].text);
			}

			_this.set_message(form_id, error_messages.join(" "));
		}
		else {
			var token = response.opaqueData.dataValue;
			$("#ezfc-authorizetoken-" + _this.payment_form_id).val(token);

			_this.form_submit($form, -1, "authorize-checkout");
		}
	};

	/**
		checks for blocked days in a date-element
	**/
	this.check_datepicker_days = function(date, available_days, blocked_days) {
		date.setHours(0,0,0,0);
		var loop_date = date.getTime();

		if (blocked_days.length) {
			for (var bd in blocked_days) {
				var is_period = blocked_days[bd].indexOf(":") !== -1;

				// date period
				if (is_period) {
					var blocked_days_period = blocked_days[bd].split(":");

					if (blocked_days_period.length == 2) {
						var blocked_days_period_begin = new Date(blocked_days_period[0]);
						var blocked_days_period_end   = new Date(blocked_days_period[1]);

						// loop date is between the period -> block date
						if (loop_date >= blocked_days_period_begin.getTime() && loop_date <= blocked_days_period_end.getTime()) {
							return [false, ""];
						}
					}
				}
				// single date
				else {
					var blocked_day = new Date(blocked_days[bd]);

					// loop date is blocked day
					if (loop_date == blocked_day.getTime()) {
						return [false, ""];
					}
				}
			}
		}

		var day = date.getDay();
		return [$.inArray(day, available_days) !== -1, ''];
	};

	/**
		get subtotal value from target
	**/
	this.get_target_subtotal_value = function(form_id, element_id, use_previous_element_price) {
		for (var i in this.subtotals[form_id]) {
			if (this.subtotals[form_id][i].el_id == element_id) {
				if (use_previous_element_price) {
					// if previous element doesn't exist, return 0
					return typeof this.subtotals[form_id][i-1] !== "undefined" ? this.subtotals[form_id][i-1].price : 0;
				}
				else {
					return this.subtotals[form_id][i].price;
				}
			}
		}

		return null;
	};

	this.get_calculated_element_value = function(form_id, element_id) {
		var $form = $("#ezfc-form-" + form_id);
		var $element = $("#ezfc_element-" + element_id);
		var el_type = $element.data("element");

		// calculations
		var calculated_value = this.calculate_element_loop(form_id, $element, el_type, 0);
		// discount
		calculated_value = this.calculate_discounts($form, form_id, $element, el_type, calculated_value);

		return calculated_value;
	};

	/**
		set message
	**/
	this.set_message = function(form_id, message) {
		var $form = $("#ezfc-form-" + form_id);
		if (!$form) return false;

		var $form_wrapper = $form.closest(".ezfc-wrapper");
		var $message_wrapper;

		// use payment message element
		if ($form_wrapper.find(".ezfc-payment-dialog-open").length > 0) {
			$message_wrapper = $form_wrapper.find(".ezfc-payment-errors");
		}
		else {
			$message_wrapper = $form.parents(".ezfc-wrapper").find(".ezfc-message");
		}

		if (!$message_wrapper.length && console) {
			console.log(message);
			return false;
		}

		$message_wrapper.text(message).fadeIn().delay(7500).fadeOut();
	};

	/**
		reload recaptcha
	**/
	this.recaptcha_reload = function() {
		var has_recaptcha = $(".ezfc-form .g-recaptcha").length;

		if (has_recaptcha && typeof grecaptcha !== "undefined") {
			grecaptcha.reset();
		}
	};

	/**
		populate html placeholders
	**/
	this.populate_html_placeholders = function($form) {
		$form.find(".ezfc-html-placeholder").each(function(i, el) {
			var $el = $(el);
			var value = "";

			var target_id = _this.functions.get_element_id_by_name($form.data("id"), $el.data("listen_target"));
			// no target found
			if (!target_id) return;

			// retrieve value
			var retrieve_value = $el.data("listen_retrieve");
			value = _this.get_value_from_element(false, target_id, true, false, { return_value: retrieve_value });

			// join values together if array
			if (typeof value === "object") value = value.join(", ");
			// trim value
			value = $.trim(value);

			$el.text(value);
		});
	};

	/**
		repeat group
	**/
	this.group_repeat = function(group_id) {
		// create group repeat id counter
		if (typeof this.group_repeat_elements[group_id] === "undefined") {
			this.group_repeat_elements[group_id] = 0;
		}
		this.group_repeat_elements[group_id]++;

		var $group = $("#ezfc_element-" + group_id);
		var $clone = $group.clone(true);
		var $form  = $group.closest(".ezfc-form");

		$clone.find(".ezfc-group-repeatable-wrapper").remove();

		// clone group
		$clone.insertAfter($group).attr("id", $clone.attr("id") + this.group_repeat_elements[group_id]);

		// change IDs
		$clone.find("[id^='ezfc_element-']").each(function() {
			var old_id    = $(this).attr("id");
			var new_id    = old_id + "-" + _this.group_repeat_elements[group_id];
			var new_fe_id = $(this).data("id") + "-" + _this.group_repeat_elements[group_id];

			$(this).attr("id", new_id);
			$(this).data("id", new_fe_id);
		});

		this.form_change($form);
		//this.init_form_ui($group.closest(".ezfc-form"), false);
	};

	/**
		toggle group
	**/
	this.toggle_group = function($group_wrapper, force_show) {
		var $group = $group_wrapper.find("> .ezfc-group-elements").first();

		if ($group_wrapper.hasClass("ezfc-group-collapsible")) {
			$group.slideToggle(500);

			var group_class_active = "ezfc-group-active";
			var icon_class_open    = "fa-chevron-circle-down";
			var icon_class_closed  = "fa-chevron-circle-right";

			var $toggle_icon = $group_wrapper.find(".ezfc-collapse-icon i").first();
			// close group
			if ($toggle_icon.hasClass(icon_class_open) && !force_show) {
				$toggle_icon.removeClass(icon_class_open).addClass(icon_class_closed);
				$group_wrapper.removeClass(group_class_active);
			}
			// open group
			else {
				$toggle_icon.removeClass(icon_class_closed).addClass(icon_class_open);
				$group_wrapper.addClass(group_class_active);
			}
		}
	};

	/**
		show tip
	**/
	this.show_tip = function(el_tip, el_target, tip_delay, message) {
		var tip = new Opentip(el_tip, {
			background: ezfc_vars.opentip.background || "yellow",
			delay: tip_delay,
			hideDelay: 0.1,
			hideTriggers: ["closeButton", "target"],
			removeElementsOnHide: true,
			showOn: null,
			target: el_target,
			tipJoint: ezfc_vars.required_text_position || "middle right",
		});

		tip.setContent(message);
		tip.show();
	};

	/**
		cancel submission
	**/
	this.submit_cancel = function($form, buttons_only) {
		var $form_wrapper   = $form.closest(".ezfc-wrapper");
		var $submit_element = $form.find("input[type='submit']");
		var $submit_icon    = $form.find(".ezfc-submit-icon");
		var id              = $form.data("id");

		$submit_icon.fadeOut();
		$submit_element.prop("disabled", false);
		$form_wrapper.find(".ezfc-payment-submit, .ezfc-payment-cancel").prop("disabled", false);

		for (var i in this.payment_methods) {
			var payment_method = _this.payment_methods[i];

			_this.form_vars[id].payment_info_shown[payment_method] = 0;
		}
	};


	/**
		debug
	**/
	this.remove_debug_info = function() {
		$(".ezfc-debug-info").remove();
	};

	this.add_debug_info = function(type, element, text) {
		if (ezfc_vars.debug_mode != 2) return;

		type = type || $(element).data("element");
		// don't show group elements
		if (type == "group") return;

		var id = $(element).attr("id");
		if (id) {
			id = id.split("ezfc_element-")[1];
		}

		var debug_el_id = "ezfc-debug-" + id;
		var debug_exists_for_element = $("#" + debug_el_id).length;

		// check if wrapper was already added
		if (type == "custom_calculation" && debug_exists_for_element) return;

		var debug_text = "[[" + type + " #" + id + "]]\n[" + type + "]\n" + text + "\n\n";
		if (debug_exists_for_element) {
			$(element).find(".ezfc-debug-info").append(debug_text);
		}
		else {
			$(element).append("<pre id='" + debug_el_id + "' class='ezfc-debug-info ezfc-debug-type-" + type + "'>" + debug_text + "</pre>");
		}

		console.log(text, element);
	};

	this.debug_message = function(message) {
		if (ezfc_vars.debug_mode == 0) return;

		console.log(message);
	};

	/**
		misc
	**/
	this.jqueryui_date_diff = function(start, end, workdays_only) {
		if (!start || !end)  return 0;

		var days = 0;

		if (workdays_only) {
			var elapsed, daysBeforeFirstSaturday, daysAfterLastSunday;
			var ifThen = function (a, b, c) {
				return a == b ? c : a;
			};

			elapsed = end - start;
			elapsed /= 86400000;

			daysBeforeFirstSunday = (7 - start.getDay()) % 7;
			daysAfterLastSunday = end.getDay();

			elapsed -= (daysBeforeFirstSunday + daysAfterLastSunday);
			elapsed = (elapsed / 7) * 5;
			elapsed += ifThen(daysBeforeFirstSunday - 1, -1, 0) + ifThen(daysAfterLastSunday, 6, 5);

			days = elapsed;
		}
		else {
			days = (end - start) / 1000 / 60 / 60 / 24;
		}

		days = Math.ceil(days);

		return days;
	};

	this.roundTo = function(number, precision) {
		var factor = Math.pow(10, precision);
		var tempNumber = number * factor;
		var roundedTempNumber = Math.round(tempNumber);
		return roundedTempNumber / factor;
	};

	this.sprintf = function() {
		var args = arguments,
	    string = args[0],
	    i = 1;
	    return string.replace(/%((%)|s|d)/g, function (m) {
	        // m is the matched format, e.g. %s, %d
	        var val = null;
	        if (m[2]) {
	            val = m[2];
	        } else {
	            val = args[i];
	            // A switch statement so that the formatter can be extended. Default is %s
	            switch (m) {
	                case '%d':
	                    val = parseFloat(val);
	                    if (isNaN(val)) {
	                        val = 0;
	                    }
	                    break;
	            }
	            i++;
	        }
	        return val;
	    });
	};

	this.throttle = function(fn, threshhold, scope) {
	  threshhold = threshhold || (threshhold = 100);
	  var last,
	      deferTimer;
	  return function () {
	    var context = scope || this;

	    var now = +new Date(),
	        args = arguments;
	    if (last && now < last + threshhold) {
	      // hold on to it
	      clearTimeout(deferTimer);
	      deferTimer = setTimeout(function () {
	        last = now;
	        fn.apply(context, args);
	      }, threshhold);
	    } else {
	      last = now;
	      fn.apply(context, args);
	    }
	  };
	};

	this.init();
};

ezfc_functions = {};
var EZFC;

jQuery(document).ready(function($) {
	EZFC = new EZFC_Object($);
});