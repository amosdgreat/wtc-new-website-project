<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wtc_website' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Okp#]W{C dxIwdN;/]l{ROBeW8qb~@jSqQzDBE^w6*R&(})I6h>iU#VezROU3.G[' );
define( 'SECURE_AUTH_KEY',  '4wTJ]PR f0B_V=RgPXvP>,R4dNE3FQK19kvV.5*?wX2_oju,xb.i5*zHMM3$1 A(' );
define( 'LOGGED_IN_KEY',    '2.a AfH&:f5U~38p:CV,T)yPznEn9Xfvs9|DDnC6Em58sD$E$!NHxDVn1DhM~9Wj' );
define( 'NONCE_KEY',        'LGFe~#q]>+.@As?Y-T(L2`wjPTbU1)I6U`ue@*;Pk#y:uk_v%2v6#Lu<Tf_/$`(9' );
define( 'AUTH_SALT',        '_q$m,NSv)|lN/bBiV( vIf80M@2@I_z3[bZ$$A(~gC26>%A)i[lP7(T9b;;)+qs2' );
define( 'SECURE_AUTH_SALT', '` QM@/`Tj=kJvk+5<6#AO3|AIytjAqb+.k4#estzq:dbjRhXX&~y+_kS,Gc Rfx8' );
define( 'LOGGED_IN_SALT',   ')~&t772Ggv}5HM/+-3qCt3%wEG~Zcy$+,)SXf!s1|u^`5>0U[VhvPgDgv9w L#0K' );
define( 'NONCE_SALT',       'L`!d(uK$V5pfkgx!3vt=u@SWG1sP< F2HThBTa!f3y+a?CG7_&$l4sf!)cXRAx!2' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
